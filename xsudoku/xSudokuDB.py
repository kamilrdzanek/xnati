from sqlite3 import connect as open_sqlite
from typing import Dict, List, Tuple
from hashlib import md5
from typing import Dict, Tuple
import json

TUPLE_FORMAT = '{t1}||{t2}'
KEY_MD5_HASH = 'md5_hash'
KEY_RANK = 'rank'
KEY_GRID = 'grid'
KEY_GRID_SOLUTION = 'grid_solution'
SPECIAL_CHAR = '||'

SQL_QUERY_CREATE_SUDOKU_TABLE = \
    "CREATE TABLE IF NOT EXISTS SUDOKU \
    ( \
        SUDOKUID INTEGER PRIMARY KEY AUTOINCREMENT, \
        HASH TEXT NOT NULL, \
        RANK INTEGER NOT NULL, \
        GRID TEXT NOT NULL, \
        GRID_SOLUTION TEXT NOT NULL, \
        STATUS INTEGER DEFAULT 0, \
        UNIQUE(HASH) \
    )"

SQL_QUERY_SELECT_ALL_SUDOKU = \
    "SELECT HASH, RANK, GRID, GRID_SOLUTION FROM SUDOKU WHERE STATUS = 0 #rand# #limit#"

SQL_QUERY_UPDATE_STATUS = \
    "UPDATE SUDOKU SET STATUS = 1 WHERE HASH = ?"

def to_json(value_dict:Dict[Tuple[int, int],int]) -> str:
    dict_to_json = {}
    for key, value in value_dict.items():
        new_key = TUPLE_FORMAT.format(t1=key[0],t2=key[1])
        dict_to_json[new_key] = value
    return json.dumps(dict_to_json, indent = 4)

def json_to_grid_dic(json_grid:str) -> Dict[Tuple[int, int], int]:
    grid_dict = {}
    if json_grid is not None and len(json_grid) > 0:
        grid_json = json.loads(json_grid)
        for k, v in grid_json.items():
            values = k.split(SPECIAL_CHAR)
            new_key = (int(values[0]), int(values[1]))
            grid_dict[new_key] = v
    return grid_dict

class XPuzzle:
    def __init__(self, rank:int, 
        grid:Dict[Tuple[int, int], int],
        grid_solution:Dict[Tuple[int, int], int], 
        md5_value:str = None
        ):
        self.rank = rank
        self.grid = grid
        self.grid_solution = grid_solution
        if md5_value is None:
            self.md5_hash = self.calculate_md5(rank, grid, grid_solution)
        else:
            self.md5_hash = md5_value
    
    def calculate_md5(self, rank:int, 
        grid:Dict[Tuple[int, int],int], 
        solution:Dict[Tuple[int, int],int]
        ):
        grid_json = to_json(grid)
        grid_solution_json = to_json(solution)
        value_to_md5 = "{rang}|{grid}|{solution}".format(rang=rank, grid=grid_json, \
            solution=grid_solution_json)
        return md5(value_to_md5.encode('utf-8')).hexdigest() 

class XSudokuDB():
    def __init__(self, path:str):
        self.path = path
        self.sql_driver = open_sqlite(path)
        self._create_structure_if_not_exists()

    def _create_structure_if_not_exists(self):
        cursor = self.sql_driver.cursor()
        cursor.executescript(SQL_QUERY_CREATE_SUDOKU_TABLE)
        self.sql_driver.commit()

    def find_all(self, limit:int = 0, random:bool = False) -> List[XPuzzle]:
        result = []
        params = []
        sql_query = SQL_QUERY_SELECT_ALL_SUDOKU
        sql_query = sql_query.replace("#rand#","ORDER BY RANDOM()") if random else sql_query.replace("#rand#","")
        limit_part = "LIMIT {value}".format(value=limit)
        sql_query = sql_query.replace("#limit#",limit_part) if limit > 0 else sql_query.replace("#limit#","")
        data = self._select_sql_query(sql_query, params)
        if len(data) > 0:
            for index, row in enumerate(data):
                md5_value = row[0]
                rank_value = row[1]
                grid_json = row[2]
                grid_solution_json = row[3]
                grid = json_to_grid_dic(grid_json)
                grid_solution = json_to_grid_dic(grid_solution_json)
                element = XPuzzle(rank_value, grid, grid_solution, md5_value)
                result.append(element)
        return result

    def update_status(self, hash:str):
        cursor = self.sql_driver.cursor()
        params = []
        params.append(hash)
        cursor.execute(SQL_QUERY_UPDATE_STATUS, params)
        self.sql_driver.commit()

    def _select_sql_query(self, sql_query:str, params:List):
        cursor = self.sql_driver.cursor()
        cursor.execute(sql_query, params)
        return cursor.fetchall()




SQL_QUERY_CREATE_SUDOKU_GEN_TABLE = \
    "CREATE TABLE IF NOT EXISTS SUDOKUGEN \
    ( \
        SUDOKUGENID INTEGER PRIMARY KEY AUTOINCREMENT, \
        HASH TEXT NOT NULL, \
        GRID_SOLUTION TEXT NOT NULL, \
        UNIQUE(HASH) \
    )"

SQL_QUERY_INSERT_SUDOKU_GEN = \
    'INSERT OR REPLACE INTO SUDOKU (HASH, RANK, GRID, GRID_SOLUTION) VALUES(?, ?, ?, ?)'

class XSudokuGenDB():
    def __init__(self, path:str):
        self.path = path
        self.sql_driver = open_sqlite(path)
        self._create_structure_if_not_exists()

    def _create_structure_if_not_exists(self):
        cursor = self.sql_driver.cursor()
        cursor.executescript(SQL_QUERY_CREATE_SUDOKU_TABLE)
        self.sql_driver.commit()

    