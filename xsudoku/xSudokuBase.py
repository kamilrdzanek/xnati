from xSudokuDB import XSudokuDB, XPuzzle
from typing import List
from math import ceil, fabs
from datetime import datetime
from copy import deepcopy
from os.path import join
from typing import Dict, Tuple
from PIL.ImageDraw import ImageDraw, Draw
from PIL.Image import Image, open as ImageOpen, new as ImageNew, ANTIALIAS
from PIL import ImageFont
import gc

try:
    from cStringIO import StringIO as BytesIO
except ImportError:
    from io import BytesIO

RESULT_DIR = '/home/rydzyk/Dokumenty/Projects/vsRepo/xnati/result/sudoku'
RES_PATH = '/home/rydzyk/Dokumenty/Projects/vsRepo/xnati/res'
SUDOKU_RES_PATH = join(RES_PATH, 'sudoku')
FONTS_RES_PATH = join(RES_PATH, 'fonts')

DB_NAME = 'Sudoku_hard.sqllite'
DB_PATH = join(SUDOKU_RES_PATH, DB_NAME)

SUDOKU_BOARD_TEMPLATE_NAME = 'Sudoku.jpg'
SUDOKU_BOARD_TEBPLATE_PATH = join(SUDOKU_RES_PATH, SUDOKU_BOARD_TEMPLATE_NAME)

FONT_NAME = 'tillana-extrabold.ttf'
FONT_PATH = join(FONTS_RES_PATH, FONT_NAME)

TITLE_POSITION = (510, 45)
PAGE_SIZE = (2550,3300)
PUZZLE_NEW_SIZE = (790, 869)
SOLUTION_NEW_SIZE = (600, 660)

FONT_SIZE = 80
FONT_CORRECT_SIZE = 70
FONT_TITLE_SIZE = 70

FONT_CHARS = ImageFont.truetype(FONT_PATH, FONT_SIZE)
FONT_CORRECT_CHARS = ImageFont.truetype(FONT_PATH, FONT_CORRECT_SIZE)
FONT_TITLE = ImageFont.truetype(FONT_PATH, FONT_TITLE_SIZE)

ARRAY_POSITION_Y = [160, 260, 365, 475, 580, 685, 790, 895, 995]
ARRAY_POSITION_X = [80, 185, 290, 400, 505, 610, 715, 815, 915]

POSITIONS_PAGE_LEFT = {0:(305, 255), 1:(1355, 255), 2:(305, 1215), 3:(1355, 1215), 4:(305, 2175), 5:(1355 ,2175)}
POSITIONS_PAGE_RIGHT = {0:(405, 255), 1:(1455, 255), 2:(405, 1215), 3:(1455, 1215), 4:(405, 2175), 5:(1455 ,2175)}
POSITIONS_SOLUTION_PAGE = {0:(305, 255), 1:(975, 255), 2:(1645, 255), 3:(305, 965), 4:(975, 965), 5:(1645, 965),
                           6:(305, 1675), 7:(975, 1675), 8:(1645, 1675), 9:(305, 2385), 10:(975, 2385), 11:(1645, 2385)}

WHITE_COLOR = 'white'
GRAY_COLOR = 'gray'

FORMT_IMG = 'jpeg'
IMG_QUALITY = 90

db = XSudokuDB(DB_PATH)
gc.enable()

def make_interior(number:int, update_sudoku_status:bool=False):
    sudoku_pages = []
    solution_pages = []
    sudoku_on_one_page = 6
    solution_on_one_page = 12
    puzzle_epoch = int(ceil(number/sudoku_on_one_page))
    puzzles = db.find_all(number, True)
    for iteration in range(0, puzzle_epoch):
        puzzles_on_page = puzzles[iteration*sudoku_on_one_page:(iteration+1)*sudoku_on_one_page]
        image = None
        number_value = iteration * sudoku_on_one_page
        if iteration % 2 == 0:
            image = make_puzzle_page(puzzles_on_page, number_value, True)
        else:
            image = make_puzzle_page(puzzles_on_page, number_value, False)
        sudoku_pages.append(image)
        print("Maked puzzle page {number}".format(number=number_value))
        gc.collect()
    puzzle_epoch = int(ceil(number/solution_on_one_page))
    for iteration in range(0, puzzle_epoch): 
        puzzles_on_page = puzzles[iteration*solution_on_one_page:(iteration+1)*solution_on_one_page]
        image = None
        number_value = iteration * solution_on_one_page
        image = make_solution_puzzle_pages(puzzles_on_page, number_value)
        solution_pages.append(image)
        print("Maked puzzle sollution page {number}".format(number=number_value))
        gc.collect()
    if update_sudoku_status:
        for item in range(0, number):
            db.update_status(puzzles[item].md5_hash)
        print("Updated selected sudoku")
    gc.collect()
    image_to_pdf = []
    for item in sudoku_pages:
        image_to_pdf.append(item)
    gc.collect()
    sudoku_pages.clear()
    for item in solution_pages:
        image_to_pdf.append(item)
    solution_pages.clear()
    gc.collect()
    now = datetime.now().replace(microsecond=0)
    file_name = "Sudoku_interior {date}.pdf".format(date=now)
    img:Image = deepcopy(image_to_pdf[0])
    path_to_save = join(RESULT_DIR, file_name)
    img.save(path_to_save, save_all=True, append_images=image_to_pdf[1:])
    print("Sudoku interior saved")

def make_sudoku_positions():
    result = {}
    for x in range(0,len(ARRAY_POSITION_X)):
        for y in range(0,len(ARRAY_POSITION_Y)):
            result[(x,y)] = (ARRAY_POSITION_X[x], ARRAY_POSITION_Y[y])
    return result

array_positions = make_sudoku_positions()
def get_signle_sudoku_board_image(grid:Dict[Tuple[int, int], int],
                                  grid_solution:Dict[Tuple[int, int], int],
                                  sudoku_number:int, 
                                  solution:bool = False,
                                  save_img:bool = False) -> Image:
    def set_title(img:ImageDraw, title:str, position:Tuple[int,int], fill:str='white') -> None:
        title_size = img.textsize(title, FONT_TITLE)
        new_pos = (position[0] - title_size[0] / 2, position[1] - title_size[1] / 2)
        img.text((new_pos[0],new_pos[1]), title, font=FONT_TITLE, fill=fill)
    def set_char_to_sudoku(position:Tuple[int, int], img_edit:ImageDraw, first_value:int, second_value:int=None):
        if first_value is not None and len(str(first_value)) > 0:
            first_value_str = str(first_value)
            char_size = img_edit.textsize(first_value_str, FONT_CHARS)
            position = array_positions[position]
            new_pos = (position[0] - char_size[0] / 2, position[1] - char_size[1] / 2)
            if first_value == second_value:
                img_edit.text(new_pos, first_value_str, font=FONT_CHARS)
            else:
                img_edit.text(new_pos, first_value_str, font=FONT_CORRECT_CHARS, fill=GRAY_COLOR)

    if grid is None or len(grid) == 0:
        return None
    img_grid = ImageOpen(SUDOKU_BOARD_TEBPLATE_PATH)
    img_grid.convert("RGB")
    img_grid_editable = Draw(img_grid)
    title = "SUDOKU {number}".format(number=sudoku_number)
    set_title(img_grid_editable, title, TITLE_POSITION, WHITE_COLOR)
    for x in range(0, 9):
        for y in range(0, 9):
            if grid_solution is None or len(grid_solution) == 0:
                sudoku_value = grid[(x,y)]
                set_char_to_sudoku((x,y), img_grid_editable, sudoku_value, sudoku_value)
            else:
                sudoku_value = grid[(x,y)]
                sudoku_solution_value = grid_solution[(x,y)]
                set_char_to_sudoku((x,y), img_grid_editable, sudoku_solution_value, sudoku_value)
    if save_img:
        format_value = "{path}/Puzzle_solution_{number}.jpg" if solution else "{path}/Puzzle_{number}.jpg"
        img_grid.save(format_value.format(path = RESULT_DIR, number=sudoku_number))
    out = BytesIO()
    img_grid.save(out, format=FORMT_IMG ,quality=IMG_QUALITY)
    out.seek(0)
    return ImageOpen(out)

def make_puzzle_page(puzzles:List[XPuzzle], number:int, left:bool):
    page_img = ImageNew("RGB", PAGE_SIZE, WHITE_COLOR)
    number_value = number
    for index, puzzle in enumerate(puzzles):
        number_value = number_value + 1
        img_puzzle = get_signle_sudoku_board_image(puzzle.grid, None, number_value, False)
        image = img_puzzle.resize(PUZZLE_NEW_SIZE, ANTIALIAS)
        image.convert("RGB")
        if left:
            page_img.paste(image, POSITIONS_PAGE_LEFT[index])
        else:
            page_img.paste(image, POSITIONS_PAGE_RIGHT[index])
    out = BytesIO()
    page_img.save(out, format=FORMT_IMG ,quality=IMG_QUALITY)
    out.seek(0)
    return ImageOpen(out)

def make_solution_puzzle_pages(puzzles:List[XPuzzle], number:int):
    page_img = ImageNew("RGB", PAGE_SIZE, WHITE_COLOR)
    number_value = number
    for index, puzzle in enumerate(puzzles):
        number_value = number_value + 1
        img_puzzle = get_signle_sudoku_board_image(puzzle.grid, puzzle.grid_solution, number_value, False)
        image = img_puzzle.resize(SOLUTION_NEW_SIZE, ANTIALIAS)
        image.convert("RGB")
        page_img.paste(image, POSITIONS_SOLUTION_PAGE[index])
    out = BytesIO()
    page_img.save(out, format=FORMT_IMG ,quality=IMG_QUALITY)
    out.seek(0)
    return ImageOpen(out)

# def get_single_sudoku(grid:Dict[Tuple[int, int], int], 
#                       grid_solution:Dict[Tuple[int, int], int],  
#                       sudoku_number:int, 
#                       save_img:bool = False) -> Tuple[Image, Image]:
#     def set_title(img:ImageDraw, title:str, position:Tuple[int,int], fill:str='white') -> None:
#         title_size = img.textsize(title, FONT_TITLE)
#         new_pos = (position[0] - title_size[0] / 2, position[1] - title_size[1] / 2)
#         img.text((new_pos[0],new_pos[1]), title, font=FONT_TITLE, fill=fill)
#     def set_char_to_sudoku(position:Tuple[int, int], img_edit:ImageDraw, first_value:int, second_value:int=None):
#         if first_value is not None and len(str(first_value)) > 0:
#             first_value_str = str(first_value)
#             char_size = img_edit.textsize(first_value_str, FONT_CHARS)
#             position = array_positions[position]
#             new_pos = (position[0] - char_size[0] / 2, position[1] - char_size[1] / 2)
#             if first_value == second_value:
#                 img_edit.text(new_pos, first_value_str, font=FONT_CHARS)
#             else:
#                 img_edit.text(new_pos, first_value_str, font=FONT_CORRECT_CHARS, fill=GRAY_COLOR)

#     if grid is None or len(grid) == 0:
#         return None, None
#     img_grid = ImageOpen(SUDOKU_BOARD_TEBPLATE_PATH)
#     img_grid.convert("RGB")
#     img_grid_editable = Draw(img_grid)
#     img_grid_solution = ImageOpen(SUDOKU_BOARD_TEBPLATE_PATH)
#     img_grid_solution.convert("RGB")
#     img_grid_solution_editable = Draw(img_grid_solution)
#     title = "SUDOKU {number}".format(number=sudoku_number)
#     set_title(img_grid_editable, title, TITLE_POSITION, WHITE_COLOR)
#     set_title(img_grid_solution_editable, title, TITLE_POSITION, WHITE_COLOR)
#     for x in range(0, 9):
#         for y in range(0, 9):
#             sudoku_value = grid[(x,y)]
#             sudoku_value_solution = grid_solution[(x,y)]
#             set_char_to_sudoku((x,y), img_grid_editable, sudoku_value, sudoku_value)
#             set_char_to_sudoku((x,y), img_grid_solution_editable, sudoku_value_solution, sudoku_value)
#     if save_img:
#         img_grid.save("{path}/Puzzle_{number}.jpg".format(path = RESULT_DIR, number=sudoku_number))
#         img_grid_solution.save("{path}/Puzzle_solution_{number}.jpg".format(path = RESULT_DIR, number=sudoku_number))
#     return (img_grid, img_grid_solution)

if __name__ == "__main__":
    make_interior(1020)