from xSudokuDB import XSudokuDB, XPuzzle
from typing import List
from math import ceil, fabs
from datetime import datetime
from copy import deepcopy
from os.path import join
from typing import Dict, Tuple
from PIL.ImageDraw import ImageDraw, Draw
from PIL.Image import Image, open as ImageOpen, new as ImageNew, ANTIALIAS
from PIL import ImageFont
from PyPDF2 import PdfFileMerger
import gc, os

try:
    from cStringIO import StringIO as BytesIO
except ImportError:
    from io import BytesIO

# Language interior
LANGUAGE = 'de'
VOL = 2
SUDOKU_TITLE = "Extrem {number}"

RESULT_DIR = '/home/rydzyk/Dokumenty/Projects/vsRepo/xnati/result/sudoku'
RES_PATH = '/home/rydzyk/Dokumenty/Projects/vsRepo/xnati/res'
SUDOKU_RES_PATH = join(RES_PATH, 'sudoku')
SUDOKU_RES_PAGES_PATH = join(SUDOKU_RES_PATH, 'pages', LANGUAGE)
FONTS_RES_PATH = join(RES_PATH, 'fonts')

DB_NAME = 'Sudoku_hard.sqllite'
DB_PATH = join(SUDOKU_RES_PATH, DB_NAME)

SUDOKU_BOARD_TEMPLATE_NAME = 'Sudoku3.jpg'
SUDOKU_BOARD_TEBPLATE_PATH = join(SUDOKU_RES_PATH, SUDOKU_BOARD_TEMPLATE_NAME)

#Special pages
FIRST_PAGE_1440_VOL1_NAME = 'First_page_1440_vol1.jpg'
FIRST_PAGE_1440_VOL1_PATH = join(SUDOKU_RES_PAGES_PATH, FIRST_PAGE_1440_VOL1_NAME)

FIRST_PAGE_1440_VOL2_NAME = 'First_page_1440_vol2.jpg'
FIRST_PAGE_1440_VOL2_PATH = join(SUDOKU_RES_PAGES_PATH, FIRST_PAGE_1440_VOL2_NAME)

FIRST_PAGE_1200_NAME = 'First_page_1200.jpg'
FIRST_PAGE_1200_PATH = join(SUDOKU_RES_PAGES_PATH, FIRST_PAGE_1200_NAME)

INSTRUCTION_PAGE_NAME = 'Instruction_page.jpg'
INSTRUCTION_PAGE_PATH = join(SUDOKU_RES_PAGES_PATH, INSTRUCTION_PAGE_NAME)

ACKNOWLEDGEMENTS_PAGE_NAME = 'Acknowledgements_page.jpg'
ACKNOWLEDGEMENTS_PAGE_PATH = join(SUDOKU_RES_PAGES_PATH, ACKNOWLEDGEMENTS_PAGE_NAME)

SPECIAL_PUZZLE_PAGE_NAME = 'Special_page_puzzles.jpg'
SPECIAL_PUZZLE_PAGE_PATH = join(SUDOKU_RES_PAGES_PATH, SPECIAL_PUZZLE_PAGE_NAME)

SPECIAL_SOLUTION_PAGE_NAME = 'Special_page_solutions.jpg'
SPECIAL_SOLUTION_PAGE_PATH = join(SUDOKU_RES_PAGES_PATH, SPECIAL_SOLUTION_PAGE_NAME)

COPYRIGHTI_PAGE_NAME = 'Copyrighti.jpg'
COPYRIGHTI_PAGE_PATH = join(SUDOKU_RES_PAGES_PATH, COPYRIGHTI_PAGE_NAME)
#End special pages

FONT_NAME = 'tillana-extrabold.ttf'
FONT_PATH = join(FONTS_RES_PATH, FONT_NAME)

FONT_SOLUTION_NAME = 'blzee.ttf'
FONT_SOLUTION_PATH = join(FONTS_RES_PATH, FONT_SOLUTION_NAME)

TITLE_POSITION = (510, 45)
PAGE_SIZE = (2550,3300)
PUZZLE_NEW_SIZE = (790, 869)
SOLUTION_NEW_SIZE = (600, 660)

FONT_SIZE = 80
FONT_CORRECT_SIZE = 70
FONT_TITLE_SIZE = 70

FONT_CHARS = ImageFont.truetype(FONT_PATH, FONT_SIZE)
FONT_CORRECT_CHARS = ImageFont.truetype(FONT_SOLUTION_PATH, FONT_CORRECT_SIZE)
FONT_TITLE = ImageFont.truetype(FONT_PATH, FONT_TITLE_SIZE)

ARRAY_POSITION_Y = [160, 260, 365, 475, 580, 685, 790, 895, 995]
ARRAY_POSITION_X = [80, 185, 290, 400, 505, 610, 715, 815, 915]

POSITIONS_PAGE_LEFT = {0:(305, 255), 1:(1355, 255), 2:(305, 1215), 3:(1355, 1215), 4:(305, 2175), 5:(1355 ,2175)}
POSITIONS_PAGE_RIGHT = {0:(405, 255), 1:(1455, 255), 2:(405, 1215), 3:(1455, 1215), 4:(405, 2175), 5:(1455 ,2175)}
POSITIONS_SOLUTION_PAGE = {0:(305, 255), 1:(975, 255), 2:(1645, 255), 3:(305, 965), 4:(975, 965), 5:(1645, 965),
                           6:(305, 1675), 7:(975, 1675), 8:(1645, 1675), 9:(305, 2385), 10:(975, 2385), 11:(1645, 2385)}

WHITE_COLOR = 'white'
GRAY_COLOR = 'gray'

FORMT_IMG = 'jpeg'
IMG_QUALITY = 90

FIRST_SPECIAL_PAGES = [ACKNOWLEDGEMENTS_PAGE_PATH, INSTRUCTION_PAGE_PATH, COPYRIGHTI_PAGE_PATH, SPECIAL_PUZZLE_PAGE_PATH]
FIRST_SPECIAL_PDF_NAME = 'first_special_pages.pdf'
SOLUTION_SPECIAL_PDF_NAME = 'solution_special_pages.pdf'

db = XSudokuDB(DB_PATH)
gc.enable()

def make_interor_ext(number:int, update_sudoku_status:bool=False):
    number_of_part_pdf = 120
    def merge_pdfs():
        puzzles_path = []
        solutions_path = []
        first_special_path = []
        solution_special_path = []
        for r, d, f in os.walk(RESULT_DIR):
            for file in f:
                if file.endswith(".pdf") \
                    and ("_puzzle_part_" in file \
                        or "_solution_part_" in file
                        or "first_special_pages" in file
                        or "solution_special_pages" in file):
                    pdf_path = os.path.join(r, file)
                    if "_puzzle_part_" in file:
                        puzzles_path.append(pdf_path)
                    if "_solution_part_" in file:
                        solutions_path.append(pdf_path)
                    if 'first_special_pages' in file:
                        first_special_path.append(pdf_path)
                    if 'solution_special_pages' in file:
                        solution_special_path.append(pdf_path)
        puzzles_path.sort()
        solutions_path.sort()
        merger = PdfFileMerger()
        for pdf in first_special_path:
            merger.append(open(pdf, 'rb'))
        for pdf in puzzles_path:
            merger.append(open(pdf, 'rb'))
        for pdf in solution_special_path:
            merger.append(open(pdf, 'rb'))
        for pdf in solutions_path:
            merger.append(open(pdf, 'rb'))
        now = datetime.now().replace(microsecond=0)
        file_name = "Sudoku_interior {date}.pdf".format(date=now)
        path_to_save = join(RESULT_DIR, file_name)
        with open(path_to_save, "wb") as fout:
            merger.write(fout)
        for pdf in first_special_path:
            os.remove(pdf)
        for pdf in puzzles_path:
            os.remove(pdf)
        for pdf in solution_special_path:
            os.remove(pdf)
        for pdf in solutions_path:
            os.remove(pdf)
    def get_first_img() -> Image:
        if (number == 1440):
            if (VOL == 1):
                return ImageOpen(FIRST_PAGE_1440_VOL1_PATH)
            if (VOL == 2):
                return ImageOpen(FIRST_PAGE_1440_VOL2_PATH)
        if (number == 1200):
            return ImageOpen(FIRST_PAGE_1200_PATH)
        else:
            return ImageOpen(FIRST_PAGE_1200_PATH)
    puzzles = db.find_all(number, True)
    pdf_epoch = int(ceil(number/number_of_part_pdf))
    first_special_images = [ImageOpen(img_path) for img_path in FIRST_SPECIAL_PAGES]
    first_special_images.insert(0, get_first_img())
    make_pdf_from_images(first_special_images, FIRST_SPECIAL_PDF_NAME)
    make_pdf_from_images([ImageOpen(SPECIAL_SOLUTION_PAGE_PATH)], SOLUTION_SPECIAL_PDF_NAME)
    for iteration in range(0, pdf_epoch):
        part_puzzles = puzzles[iteration*number_of_part_pdf:(iteration+1)*number_of_part_pdf]
        make_part_interor(part_puzzles, iteration, number_of_part_pdf, update_sudoku_status)
    merge_pdfs()

def make_pdf_from_images(images:List[Image], file_name:str="first_special_pages.pdf"):
    images_to_pdf = []
    for img in images:
        images_to_pdf.append(img)
    path_puzzle_to_save = join(RESULT_DIR, file_name)
    img_puzzle:Image = deepcopy(images_to_pdf[0])
    img_puzzle.save(path_puzzle_to_save, save_all=True, append_images=images_to_pdf[1:])

def make_part_interor(puzzles:List[XPuzzle], start_number:int=0, number_of_part_pdf:int=120, update_sudoku_status:bool=False):
    sudoku_on_one_page = 6
    solution_on_one_page = 12
    sudoku_pages = []
    solution_pages = []
    part_number = len(puzzles)
    puzzle_epoch = int(ceil(part_number/sudoku_on_one_page))
    for iteration in range(0, puzzle_epoch):
        puzzles_on_page = puzzles[iteration*sudoku_on_one_page:(iteration+1)*sudoku_on_one_page]
        image = None
        number_value = (start_number * number_of_part_pdf) + (iteration * sudoku_on_one_page)
        if iteration % 2 == 0:
            image = make_puzzle_page(puzzles_on_page, number_value, True)
        else:
            image = make_puzzle_page(puzzles_on_page, number_value, False)
        sudoku_pages.append(image)
        print("Maked puzzle page {number}".format(number=number_value))
        gc.collect()
    puzzle_epoch = int(ceil(part_number/solution_on_one_page))
    for iteration in range(0, puzzle_epoch): 
        puzzles_on_page = puzzles[iteration*solution_on_one_page:(iteration+1)*solution_on_one_page]
        image = None
        number_value = (start_number * number_of_part_pdf) + (iteration * solution_on_one_page)
        image = make_solution_puzzle_pages(puzzles_on_page, number_value)
        solution_pages.append(image)
        print("Maked puzzle sollution page {number}".format(number=number_value))
        gc.collect()
    if update_sudoku_status:
        for item in range(0, part_number):
            db.update_status(puzzles[item].md5_hash)
        print("Updated selected sudoku")
    gc.collect()
    img_puzzles_to_pdf = []
    img_sullution_to_pdf = []
    for item in sudoku_pages:
        img_puzzles_to_pdf.append(item)
    gc.collect()
    sudoku_pages.clear()
    for item in solution_pages:
        img_sullution_to_pdf.append(item)
    solution_pages.clear()
    gc.collect()
    file_name_puzzle = "Sudoku_interior_puzzle_part_{number}.pdf".format(number="%06d" % start_number)
    path_puzzle_to_save = join(RESULT_DIR, file_name_puzzle)
    img_puzzle:Image = deepcopy(img_puzzles_to_pdf[0])
    img_puzzle.save(path_puzzle_to_save, save_all=True, append_images=img_puzzles_to_pdf[1:])
    file_name_solution = "Sudoku_interior_solution_part_{number}.pdf".format(number="%06d" % start_number)
    path_solution_to_save = join(RESULT_DIR, file_name_solution)
    img_solution:Image = deepcopy(img_sullution_to_pdf[0])
    img_solution.save(path_solution_to_save, save_all=True, append_images=img_sullution_to_pdf[1:])

def make_interior(number:int, update_sudoku_status:bool=False):
    sudoku_pages = []
    solution_pages = []
    sudoku_on_one_page = 6
    solution_on_one_page = 12
    puzzle_epoch = int(ceil(number/sudoku_on_one_page))
    puzzles = db.find_all(number, True)
    for iteration in range(0, puzzle_epoch):
        puzzles_on_page = puzzles[iteration*sudoku_on_one_page:(iteration+1)*sudoku_on_one_page]
        image = None
        number_value = iteration * sudoku_on_one_page
        if iteration % 2 == 0:
            image = make_puzzle_page(puzzles_on_page, number_value, True)
        else:
            image = make_puzzle_page(puzzles_on_page, number_value, False)
        sudoku_pages.append(image)
        print("Maked puzzle page {number}".format(number=number_value))
        gc.collect()
    puzzle_epoch = int(ceil(number/solution_on_one_page))
    for iteration in range(0, puzzle_epoch): 
        puzzles_on_page = puzzles[iteration*solution_on_one_page:(iteration+1)*solution_on_one_page]
        image = None
        number_value = iteration * solution_on_one_page
        image = make_solution_puzzle_pages(puzzles_on_page, number_value)
        solution_pages.append(image)
        print("Maked puzzle sollution page {number}".format(number=number_value))
        gc.collect()
    if update_sudoku_status:
        for item in range(0, number):
            db.update_status(puzzles[item].md5_hash)
        print("Updated selected sudoku")
    gc.collect()
    image_to_pdf = []
    for item in sudoku_pages:
        image_to_pdf.append(item)
    gc.collect()
    sudoku_pages.clear()
    for item in solution_pages:
        image_to_pdf.append(item)
    solution_pages.clear()
    gc.collect()
    now = datetime.now().replace(microsecond=0)
    file_name = "Sudoku_interior {date}.pdf".format(date=now)
    img:Image = deepcopy(image_to_pdf[0])
    path_to_save = join(RESULT_DIR, file_name)
    img.save(path_to_save, save_all=True, append_images=image_to_pdf[1:])
    print("Sudoku interior saved")

def make_sudoku_positions():
    result = {}
    for x in range(0,len(ARRAY_POSITION_X)):
        for y in range(0,len(ARRAY_POSITION_Y)):
            result[(x,y)] = (ARRAY_POSITION_X[x], ARRAY_POSITION_Y[y])
    return result

array_positions = make_sudoku_positions()
def get_signle_sudoku_board_image(grid:Dict[Tuple[int, int], int],
                                  grid_solution:Dict[Tuple[int, int], int],
                                  sudoku_number:int, 
                                  solution:bool = False,
                                  save_img:bool = False) -> Image:
    def set_title(img:ImageDraw, title:str, position:Tuple[int,int], fill:str='white') -> None:
        title_size = img.textsize(title, FONT_TITLE)
        new_pos = (position[0] - title_size[0] / 2, position[1] - title_size[1] / 2)
        img.text((new_pos[0],new_pos[1]), title, font=FONT_TITLE, fill=fill)
    def set_char_to_sudoku(position:Tuple[int, int], img_edit:ImageDraw, first_value:int, second_value:int=None):
        if first_value is not None and len(str(first_value)) > 0:
            first_value_str = str(first_value)
            char_size = img_edit.textsize(first_value_str, FONT_CHARS)
            position = array_positions[position]
            new_pos = (position[0] - char_size[0] / 2, position[1] - char_size[1] / 2)
            if first_value == second_value:
                img_edit.text(new_pos, first_value_str, font=FONT_CHARS)
            else:
                # eager - font
                # special_pos = [new_pos[0], new_pos[1] + 20]
                # img_edit.text(special_pos, first_value_str, font=FONT_CORRECT_CHARS, fill=GRAY_COLOR)
                # blzee - font
                if (first_value_str in ['7', '9']):
                    img_edit.text(new_pos, first_value_str, font=FONT_CORRECT_CHARS, fill=GRAY_COLOR)
                else:
                    special_pos = [new_pos[0], new_pos[1] + 15]
                    img_edit.text(special_pos, first_value_str, font=FONT_CORRECT_CHARS, fill=GRAY_COLOR)
    if grid is None or len(grid) == 0:
        return None
    img_grid = ImageOpen(SUDOKU_BOARD_TEBPLATE_PATH)
    img_grid.convert("RGB")
    img_grid_editable = Draw(img_grid)
    title = SUDOKU_TITLE.format(number=sudoku_number)
    set_title(img_grid_editable, title, TITLE_POSITION, WHITE_COLOR)
    for x in range(0, 9):
        for y in range(0, 9):
            if grid_solution is None or len(grid_solution) == 0:
                sudoku_value = grid[(x,y)]
                set_char_to_sudoku((x,y), img_grid_editable, sudoku_value, sudoku_value)
            else:
                sudoku_value = grid[(x,y)]
                sudoku_solution_value = grid_solution[(x,y)]
                set_char_to_sudoku((x,y), img_grid_editable, sudoku_solution_value, sudoku_value)
    if save_img:
        format_value = "{path}/Puzzle_solution_{number}.jpg" if solution else "{path}/Puzzle_{number}.jpg"
        img_grid.save(format_value.format(path = RESULT_DIR, number=sudoku_number))
    out = BytesIO()
    img_grid.save(out, format=FORMT_IMG ,quality=IMG_QUALITY)
    out.seek(0)
    return ImageOpen(out)

def make_puzzle_page(puzzles:List[XPuzzle], number:int, left:bool):
    page_img = ImageNew("RGB", PAGE_SIZE, WHITE_COLOR)
    number_value = number
    for index, puzzle in enumerate(puzzles):
        number_value = number_value + 1
        img_puzzle = get_signle_sudoku_board_image(puzzle.grid, None, number_value, False)
        image = img_puzzle.resize(PUZZLE_NEW_SIZE, ANTIALIAS)
        image.convert("RGB")
        if left:
            page_img.paste(image, POSITIONS_PAGE_LEFT[index])
        else:
            page_img.paste(image, POSITIONS_PAGE_RIGHT[index])
    out = BytesIO()
    page_img.save(out, format=FORMT_IMG ,quality=IMG_QUALITY)
    out.seek(0)
    return ImageOpen(out)

def make_solution_puzzle_pages(puzzles:List[XPuzzle], number:int):
    page_img = ImageNew("RGB", PAGE_SIZE, WHITE_COLOR)
    number_value = number
    for index, puzzle in enumerate(puzzles):
        number_value = number_value + 1
        img_puzzle = get_signle_sudoku_board_image(puzzle.grid, puzzle.grid_solution, number_value, False)
        image = img_puzzle.resize(SOLUTION_NEW_SIZE, ANTIALIAS)
        image.convert("RGB")
        page_img.paste(image, POSITIONS_SOLUTION_PAGE[index])
    out = BytesIO()
    page_img.save(out, format=FORMT_IMG ,quality=IMG_QUALITY)
    out.seek(0)
    return ImageOpen(out)

if __name__ == "__main__":
    # make_interor_ext(144)
    make_interor_ext(1200)
