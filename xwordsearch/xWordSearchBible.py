from hashlib import md5
from typing import Tuple, List
from copy import deepcopy
from PIL import Image, ImageDraw, ImageFont
from PIL.Image import open as ImageOpen
from os.path import join
from datetime import datetime
from PyPDF2 import PdfFileMerger
import random
import math
import os

MAX_X = 20
MAX_Y = 20
ALLOW_BACKWARDS_WORDS = False

COLOR_BLACK = (0,0,0)
COLOR_GRAY = (128,128,128)
COLOR_WHITE = (255,255,255)

PUZZLE_NAME_FORMAT = "BIBLE VERSE {number}"

DIR_INPUT_FONT_RES = '/home/rydzyk/Dokumenty/Projects/vsRepo/xnati/res/fonts/tillana-extrabold.ttf'
DIR_INPUT_WS_PAGES_RES = '/home/rydzyk/Dokumenty/Projects/vsRepo/xnati/res/word_search'
DIR_INPUT_WS_PAGES_BIBLE_RES = '/home/rydzyk/Dokumenty/Projects/vsRepo/xnati/res/word_search/bible_edition'

DIR_RESULT_WS = "/home/rydzyk/Dokumenty/Projects/vsRepo/xnati/result/word_search"

SPECIAL_PAGE_FIRST = join(DIR_INPUT_WS_PAGES_BIBLE_RES, 'First.jpg')
SPECIAL_PAGE_ACKNOWLEDGEMENTS = join(DIR_INPUT_WS_PAGES_BIBLE_RES, 'Acknowledgements_page.jpg')
SPECIAL_PAGE_INSTRUCTION = join(DIR_INPUT_WS_PAGES_BIBLE_RES, 'Instruction_page.jpg')
SPECIAL_PAGE_COPYRIGHT = join(DIR_INPUT_WS_PAGES_BIBLE_RES, 'Copyright_eng.jpg')
SPECIAL_PAGE_PUZZLE = join(DIR_INPUT_WS_PAGES_BIBLE_RES, 'Puzzles.jpg')
SPECIAL_PAGE_SOLUTION = join(DIR_INPUT_WS_PAGES_BIBLE_RES, 'Solutions.jpg')

TEMPLATE_ONE_PAGE_WS = join(DIR_INPUT_WS_PAGES_BIBLE_RES, 'Template_standard.jpg')
TEMPLATE_VERSE_PAGE_WS = join(DIR_INPUT_WS_PAGES_BIBLE_RES, 'Template_verse_2.jpg')
TEMPLATE_TWO_PAGE_WS_1 = join(DIR_INPUT_WS_PAGES_RES, 'Template_extension_1.jpg')
TEMPLATE_TWO_PAGE_WS_2 = join(DIR_INPUT_WS_PAGES_RES, 'Template_extension_2.jpg')
TEMPLATE_ANSWER_PAGE_WS_1 = join(DIR_INPUT_WS_PAGES_BIBLE_RES, 'Template_answer_6.jpg')
TEMPLATE_ANSWER_PAGE_WS_2 = join(DIR_INPUT_WS_PAGES_BIBLE_RES, 'Template_answer_6.jpg')
TEMPLATE_ANSWER_PAGE_WS_3 = join(DIR_INPUT_WS_PAGES_BIBLE_RES, 'Template_answer_6.jpg')
TEMPLATE_ANSWER_PAGE_WS_4 = join(DIR_INPUT_WS_PAGES_BIBLE_RES, 'Template_answer_6.jpg')
TEMPLATE_ANSWER_PAGE_WS_5 = join(DIR_INPUT_WS_PAGES_BIBLE_RES, 'Template_answer_6.jpg')
TEMPLATE_ANSWER_PAGE_WS_6 = join(DIR_INPUT_WS_PAGES_BIBLE_RES, 'Template_answer_6.jpg')
TEMPLATE_ANSWER_PAGES_WS = [
    TEMPLATE_ANSWER_PAGE_WS_1, TEMPLATE_ANSWER_PAGE_WS_2, TEMPLATE_ANSWER_PAGE_WS_3,
    TEMPLATE_ANSWER_PAGE_WS_4, TEMPLATE_ANSWER_PAGE_WS_5, TEMPLATE_ANSWER_PAGE_WS_6
    ]

FONT_SIZE_20 = ImageFont.truetype(DIR_INPUT_FONT_RES, 20)
FONT_SIZE_26 = ImageFont.truetype(DIR_INPUT_FONT_RES, 26)
FONT_SIZE_30 = ImageFont.truetype(DIR_INPUT_FONT_RES, 30)
FONT_SIZE_34 = ImageFont.truetype(DIR_INPUT_FONT_RES, 34)
FONT_SIZE_36 = ImageFont.truetype(DIR_INPUT_FONT_RES, 36)
FONT_SIZE_40 = ImageFont.truetype(DIR_INPUT_FONT_RES, 40)
FONT_SIZE_45 = ImageFont.truetype(DIR_INPUT_FONT_RES, 45)
FONT_SIZE_50 = ImageFont.truetype(DIR_INPUT_FONT_RES, 50)
FONT_SIZE_55 = ImageFont.truetype(DIR_INPUT_FONT_RES, 55)
FONT_SIZE_80 = ImageFont.truetype(DIR_INPUT_FONT_RES, 80)
FONT_SIZE_100 = ImageFont.truetype(DIR_INPUT_FONT_RES, 100)
FONT_SIZE_120 = ImageFont.truetype(DIR_INPUT_FONT_RES, 120)

DB_PATH = '/home/rydzyk/Dokumenty/Projects/vsRepo/xnati/database/Bible.sqllite'

ENGLISH_ALPHABET = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']
WORD_OLWAYS_ENABLE = ['GOD','SON','JESUS','KINGDOM', 'SPOKE', 'ANGEL', 'ANGELS', ]

class XVerseWS():
    def __init__(self, verse:str, text:str) -> None:
        self.verse = verse
        self.text = text

class XWS():
    def __init__(self,
                 ws_number:int,
                 grid:List[List[str]],
                 grid_answer:List[List[str]],
                 input_data:List[str],
                 verse:XVerseWS) -> None:
        self.ws_number = ws_number
        self.grid = grid
        self.grid_answer = grid_answer
        self.input_data = input_data
        self.verse = verse

def get_alphabet_from_words(input_data:List[XVerseWS]):
    result = []
    for word in input_data:
        for char in word.text.replace(' ', '').upper():
            if char not in result and char in ENGLISH_ALPHABET:
                result.append(char)
    return result

def generate_ws(words:List[str], alphabet:List[str]):
    grid = [['*']*MAX_X for _ in range(MAX_Y)]
    for word in words:
        _place_word(grid, word)
    grid_answer = deepcopy(grid)
    for irow in range(MAX_X):
        for icol in range(MAX_Y):
            if grid[irow][icol] == '*':
                grid[irow][icol] = random.choice(alphabet)
    return grid, grid_answer

def get_words_from_verse(verse:XVerseWS, count_of_words_per_ws:int) -> List[str]:
    words_to_remove = ['the']
    words = [w for w in verse.text.split(' ') if len(w) > 2]
    words_result = []
    tmp_words = []
    for w in words:
        if (w.lower() in words_to_remove):
            continue
        tmp_value = correct_word(w)
        if (len(tmp_value) <= 2):
            continue
        if (tmp_value not in words_result):
            words_result.append(tmp_value)
        if (tmp_value in WORD_OLWAYS_ENABLE and tmp_value not in tmp_words):
            tmp_words.append(tmp_value)
    if (len(tmp_words) >= count_of_words_per_ws):
        tmp_words.sort(key=len, reverse=True)
        return tmp_words[:count_of_words_per_ws]
    words_result.sort(key=len, reverse=True)
    if (len(tmp_words) == 0):
        return words_result[:count_of_words_per_ws]
    words_result = words_result[:count_of_words_per_ws-len(tmp_words)]
    for w in tmp_words:
        words_result.append(w)
        words_result.sort(key=len, reverse=True)
    return words_result

def correct_word(word:str):
    chars_to_replace = []
    for c in word.upper():
        if (c not in ENGLISH_ALPHABET):
            chars_to_replace.append(c)
    if (len(chars_to_replace)==0):
        return word.upper()
    else:
        tmp_value = word.upper()
        for c in chars_to_replace:
            tmp_value = tmp_value.replace(c, '')
        return tmp_value

def make_interior(input_data:List[XVerseWS],
                  count_of_words_per_ws:int=18,
                  alphabet:List[str]=None):
    alphabet_value = alphabet if alphabet is not None else get_alphabet_from_words(input_data)
    puzzle_number = 1
    puzzles = []
    for item in input_data:
        words = get_words_from_verse(item, count_of_words_per_ws)
        grid, grid_answer = generate_ws(words, alphabet_value)
        element = XWS(puzzle_number, grid, grid_answer, words, item)
        puzzles.append(element)
        puzzle_number += 1
    hash_value = md5(str(datetime.now()).encode('utf-8')).hexdigest() 
    '''Special pages'''
    special_pages = [ImageOpen(SPECIAL_PAGE_FIRST), ImageOpen(SPECIAL_PAGE_ACKNOWLEDGEMENTS), 
    ImageOpen(SPECIAL_PAGE_INSTRUCTION), ImageOpen(SPECIAL_PAGE_COPYRIGHT)]
    location_to_save = join(DIR_RESULT_WS, "WS_SPECIAL_PAGES_%s.pdf" % hash_value[:6])
    img_puzzle:Image = deepcopy(special_pages[0])
    img_puzzle.save(location_to_save, save_all=True, append_images=special_pages[1:])
    '''Large WS'''
    one_page_imgs = make_verse_ws_one_page(puzzles)
    one_page_imgs.insert(0, ImageOpen(SPECIAL_PAGE_PUZZLE))
    location_to_save = join(DIR_RESULT_WS, "WS_BIBLE_ONE_%s.pdf" % hash_value[:6])
    img_puzzle:Image = deepcopy(one_page_imgs[0])
    img_puzzle.save(location_to_save, save_all=True, append_images=one_page_imgs[1:])
    # # two_page_imgs = make_ws_two_page(puzzles)
    # # location_to_save = join(DIR_RESULT_WS, "WS_TWO_%s.pdf" % hash_value[:6])
    # # img_puzzle:Image = deepcopy(two_page_imgs[0])
    # # img_puzzle.save(location_to_save, save_all=True, append_images=two_page_imgs[1:])
    '''Solution'''
    solution_page_imgs = make_ws_solution_page(puzzles)
    solution_page_imgs.insert(0, ImageOpen(SPECIAL_PAGE_SOLUTION))
    location_to_save = join(DIR_RESULT_WS, "WS_SOLUTION_%s.pdf" % hash_value[:6])
    img_puzzle:Image = deepcopy(solution_page_imgs[0])
    img_puzzle.save(location_to_save, save_all=True, append_images=solution_page_imgs[1:])
    make_pdf()

def make_pdf(pdf_result_name:str='final_pdf.pdf'):
    pdfs = []
    bible_pdf = solution_pdf = special_pdf = None
    for r, d, f in os.walk(DIR_RESULT_WS):
        for file in f:
            if (file.endswith(".pdf")):
                if ('WS_BIBLE_ONE_' in file): 
                    bible_pdf = os.path.join(r, file)
                if ('WS_SOLUTION_' in file):
                    solution_pdf = os.path.join(r, file)
                if ('WS_SPECIAL_PAGES_' in file):
                    special_pdf = os.path.join(r, file)
    pdfs.append(special_pdf)
    pdfs.append(bible_pdf)
    pdfs.append(solution_pdf)
    merger = PdfFileMerger()
    for path in pdfs:
        merger.append(open(path, 'rb'))
    path_to_save = join(DIR_RESULT_WS, pdf_result_name)
    with open(path_to_save, "wb") as fout:
        merger.write(fout)
    for pdf in pdfs:
        os.remove(pdf)

def make_verse_ws_one_page(puzzles:List[XWS]):
    images = []
    for index, ws_puzzle in enumerate(puzzles):
        img_verse = make_veser_page(ws_puzzle)
        images.append(img_verse)
        img_ws = make_ws_one_page(ws_puzzle)
        images.append(img_ws)
    return images

def make_veser_page(ws_puzzle:XWS):
    puzzle_name = '[%s] %s' % (ws_puzzle.ws_number, ws_puzzle.verse.verse)
    img = Image.open(TEMPLATE_VERSE_PAGE_WS) 
    img_editable = ImageDraw.Draw(img)
    '''Add title'''
    pos_x = 2750 / 2
    text_size_w, text_size_h = img_editable.textsize(puzzle_name, font=FONT_SIZE_120)
    pos_x = pos_x - (text_size_w / 2)
    title_positions = (pos_x, 320)
    img_editable.text(title_positions, puzzle_name, fill=COLOR_WHITE, font=FONT_SIZE_100)
    '''Add verse'''
    x1 = (2550 - 400 * 2)
    y1 = (3300 - 800 - 800)
    run_loop = True
    fresh_sentence = ''
    font_size = 80
    while (run_loop):
        result_font = ImageFont.truetype(DIR_INPUT_FONT_RES, font_size)
        sum_value = 0
        for letter in ws_puzzle.verse.text:
            sum_value += img_editable.textsize(letter, font=result_font)[0]
        average_length_of_letter = sum_value / len(ws_puzzle.verse.text)
    
        number_of_letters_for_each_line = (x1 / 1.618) / average_length_of_letter
        incrementer = 0
        fresh_sentence = ''

        for letter in ws_puzzle.verse.text:
            if letter == '-':
                fresh_sentence += '\n\n' + letter
            elif incrementer < number_of_letters_for_each_line:
                fresh_sentence += letter
            else:
                if letter == ' ':
                    fresh_sentence += '\n'
                    incrementer = 0
                else:
                    fresh_sentence += letter
            incrementer += 1

        dim_w, dim_h = img_editable.textsize(fresh_sentence, font=result_font)
        if (dim_h > y1):
            font_size -= 2
        else:
            run_loop = False

    result_font = ImageFont.truetype(DIR_INPUT_FONT_RES, font_size)
    dim_w, dim_h = img_editable.textsize(fresh_sentence, font=result_font)
    qx = (x1 / 2 - dim_w / 2) + 400
    qy = (y1 / 2 - dim_h / 2) + 800
    img_editable.text((qx, qy), fresh_sentence, align='center', font=result_font, fill=COLOR_WHITE)
    return img

def make_ws_one_page(ws_puzzle:XWS):
    puzzle_name = '[%s] %s' % (ws_puzzle.ws_number, ws_puzzle.verse.verse.upper())
    img = Image.open(TEMPLATE_ONE_PAGE_WS)
    img_editable = ImageDraw.Draw(img)
    '''Add title'''
    pos_x = 2550 / 2
    text_size_w, text_size_h = img_editable.textsize(puzzle_name, font=FONT_SIZE_100)
    pos_x = pos_x - (text_size_w / 2)
    title_positions = (pos_x, 300)
    img_editable.text(title_positions, puzzle_name, fill=COLOR_BLACK, font=FONT_SIZE_100)
    '''Add chars'''
    char_start_position = (480, 680)
    for row in range(len(ws_puzzle.grid)):
        for col in range(len(ws_puzzle.grid[row])):
            img_editable.text((char_start_position[0] + col * 83, char_start_position[1] + row * 70), \
            ws_puzzle.grid[row][col], fill=COLOR_BLACK, font=FONT_SIZE_55)
    """Add words"""
    x_one_word_value = 320 + 955 / 2
    x_two_word_value = 320 + 955 + 955 / 2
    y_word_value = 2180
    for index, element in enumerate(ws_puzzle.input_data):
        text_size_w, text_size_h = img_editable.textsize(element, font=FONT_SIZE_50)
        if index % 2 == 0:
            position = (
                x_one_word_value - text_size_w / 2,
                y_word_value
            )
            img_editable.text(position, element, \
                fill=COLOR_BLACK, font=FONT_SIZE_50)
        else:
            position = (
                x_two_word_value - text_size_w / 2,
                y_word_value
            )
            img_editable.text(position, element, \
                fill=COLOR_BLACK, font=FONT_SIZE_50)
            y_word_value = y_word_value + 85
    return img

def make_ws_two_page(puzzles:List[XWS]):
    first_title_position = (1000, 240)
    first_char_position = (880, 360)
    first_word_position = (350, 360)
    second_title_position = (1000, 1660)
    second_char_position = (300, 1780)
    second_word_position = (1800, 1780)
    NUMBER_OF_IMAGES = 2
    number_of_epoch = math.ceil(float(len(puzzles)) / NUMBER_OF_IMAGES)
    images = []
    def make_extension_page(title_position:Tuple[int, int], 
                            char_position:Tuple[int, int], 
                            words_start_position:Tuple[int, int], 
                            puzzle:XWS):
        puzzle_name = PUZZLE_NAME_FORMAT.format(number=puzzle.ws_number)
        img_editable.text(title_position, puzzle_name, \
            fill=COLOR_BLACK, font=FONT_SIZE_80)
        for row in range(len(puzzle.grid)):
            for col in range(len(puzzle.grid[row])):
                img_editable.text((char_position[0] + col * 70, char_position[1] + row * 59), \
                    puzzle.grid[row][col], fill=COLOR_BLACK, font=FONT_SIZE_45)
        y_word_value = words_start_position[1]
        for word in puzzle.input_data:
            img_editable.text((words_start_position[0], y_word_value), \
                word, fill=COLOR_BLACK, font=FONT_SIZE_30)
            y_word_value = y_word_value + 82
    for index in range(number_of_epoch):
        ws_part = puzzles[index * NUMBER_OF_IMAGES:]
        ws_part = ws_part[:NUMBER_OF_IMAGES]
        ws_puzzle_first = ws_part[0]
        ws_puzzle_second = None
        template = TEMPLATE_TWO_PAGE_WS_1
        if (len(ws_part)>1):
            ws_puzzle_second = ws_part[1]
            template = TEMPLATE_TWO_PAGE_WS_2
        img = Image.open(template)
        img_editable = ImageDraw.Draw(img)
        make_extension_page(first_title_position, first_char_position, first_word_position, ws_puzzle_first)
        if (ws_puzzle_second is not None):
            make_extension_page(second_title_position, second_char_position, second_word_position, ws_puzzle_second)
        print("Maked extension image puzzle number: {number}".format(number=ws_puzzle_first.ws_number))
        # location_to_save = join(DIR_RESULT_WS, "PUZZLE_B_%s.jpg" % ws_puzzle_first.ws_number)
        # img.save(location_to_save)
        images.append(img)
    return images

def make_ws_solution_page(puzzles:List[XWS]):
    NUMBER_OF_IMAGES = 6
    number_of_epoch = math.ceil(float(len(puzzles)) / NUMBER_OF_IMAGES)
    images = []
    title_position_one = (825, 460)
    title_position_two = (1727, 1250)
    y_title_three = 2040
    char_position_one = (490, 520)
    char_position_two = (1390, 1310)
    y_char_three = 2100
    def get_positions(index:int):
        if index == 0:
            return (title_position_one[0], title_position_one[1], char_position_one[0], char_position_one[1])
        elif index == 1:
            return (title_position_two[0], title_position_one[1], char_position_two[0], char_position_one[1])
        elif index == 2:
            return (title_position_one[0], title_position_two[1], char_position_one[0], char_position_two[1])
        elif index == 3:
            return (title_position_two[0], title_position_two[1], char_position_two[0], char_position_two[1])
        elif index == 4:
            return (title_position_one[0], y_title_three, char_position_one[0], y_char_three)
        else:
            return (title_position_two[0], y_title_three, char_position_two[0], y_char_three)
    for index in range(number_of_epoch):
        ws_part = puzzles[index * NUMBER_OF_IMAGES:]
        ws_part = ws_part[:NUMBER_OF_IMAGES]
        template = TEMPLATE_ANSWER_PAGES_WS[len(ws_part)-1]
        img = Image.open(template)
        img_editable = ImageDraw.Draw(img)
        for index, puzzle in enumerate(ws_part):
            puzzle_name = '[%s] %s' % (puzzle.ws_number, puzzle.verse.verse)
            x_title_value, y_title_value, x_char_value, y_char_value = get_positions(index)
            text_size_w, text_size_h = img_editable.textsize(puzzle_name, font=FONT_SIZE_40)
            x_title_value = x_title_value - (text_size_w / 2)
            img_editable.text((x_title_value, y_title_value), puzzle_name, fill=COLOR_BLACK, font=FONT_SIZE_40)
            for row in range(len(puzzle.grid)):
                for col in range(len(puzzle.grid[row])):
                    char_answer = puzzle.grid_answer[row][col]
                    color_char = (char_answer != '*' and COLOR_BLACK or COLOR_GRAY)
                    font = (char_answer != '*' and FONT_SIZE_34 or FONT_SIZE_26)
                    img_editable.text((x_char_value + col * 34, y_char_value + row * 32), \
                        puzzle.grid[row][col], fill=color_char, font=font)
        print("Maked answer image puzzle number: {number}".format(number=ws_part[0].ws_number))
        # location_to_save = join(DIR_RESULT_WS, "PUZZLE_Q_%s.jpg" % ws_part[0].ws_number)
        # img.save(location_to_save)
        images.append(img)
    return images

def _place_word(grid:List[List[str]], word:str):
    """Place word randomly in the grid and return True, if possible."""
    # Left, down, and the diagonals.
    dxdy_choices = [(0,1), (1,0), (1,1), (1,-1)]
    random.shuffle(dxdy_choices)
    for (dx, dy) in dxdy_choices:
        if  ALLOW_BACKWARDS_WORDS and random.choice([True, False]):
                # If backwards words are allowed, simply reverse word.
                word = word[::-1]
        # Work out the minimum and maximum column and row indexes, given
        # the word length.
        n = len(word)
        colmin = 0
        colmax = MAX_Y - n if dx else MAX_Y - 1
        rowmin = 0 if dy >= 0 else n - 1
        rowmax = MAX_X - n if dy >= 0 else MAX_X - 1
        if colmax - colmin < 0 or rowmax - rowmin < 0:
            # No possible place for the word in this orientation.
            continue
        # Build a list of candidate locations for the word.
        candidates = []
        for irow in range(rowmin, rowmax+1):
            for icol in range(colmin, colmax+1):
                if _test_candidate(grid, irow, icol, dx, dy, word):
                    candidates.append((irow, icol))
        # If we don't have any candidates, try the next orientation.
        if not candidates:
            continue
        # Pick a random candidate location and place the word in this
        # orientation.
        loc = irow, icol = random.choice(candidates)
        for j in range(n):
            grid[irow][icol] = word[j]
            irow += dy
            icol += dx
        # We're done: no need to try any more orientations.
        break
    else:
        # If we're here, it's because we tried all orientations but
        # couldn't find anywhere to place the word. Oh dear.
        return None
    # print(word, loc, (dx, dy))
    return grid

def _test_candidate(grid:List[List[str]], irow, icol, dx, dy, word):
    """Test the candidate location (icol, irow) for word in orientation
        dx, dy)."""
    for j in range(len(word)):
        if grid[irow][icol] not in ('*', word[j]):
            return False
        irow += dy
        icol += dx
    return True

###########################################################

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, String, Integer, Boolean
from sqlalchemy.orm import sessionmaker, Session
from sqlalchemy.sql.expression import func

base = declarative_base()

class Verse(base):
    __tablename__ = 'Verse'

    verse_id = Column('verse_id', Integer, primary_key=True, autoincrement=True)
    verse = Column('verse', String, nullable=False, index=True, unique=True)
    hash_verse = Column('hash_verse', String, nullable=False, unique=True)
    text = Column('text', String, nullable=False, index=True, unique=True)
    hash_text = Column('hash_text', String, nullable=False, unique=True)
    status = Column('status', Boolean, default=True)
    all_len = Column('all_len', Integer, nullable=False)
    all_word_count = Column('all_word_count', Integer, nullable=False)

    def __init__(self, verse:str, text:str) -> None:
        self.verse = verse.lstrip().rstrip()
        self.text = text.lstrip().rstrip()
        self.all_len = len(self.text)
        self.all_word_count = len(self.text.split(' '))


class XBibleDB():
    def __init__(self, path:str) -> None:
        self.path = path
        self.engine = create_engine('sqlite:///%s' % path, echo=True)
        self.sessionmaker = sessionmaker(bind=self.engine, autocommit=True)
    
    def find_all(self, limit:int=1000, min_count:int=40, max_count=1000):
        session:Session = self.sessionmaker()
        elements = session.query(Verse).filter_by(status=True) \
            .filter(Verse.all_word_count.between(min_count, max_count)) \
            .order_by(func.random()).limit(limit).all()
        if elements is not None and len(elements) > 0:
            return elements

    def update_status(self, verse_id:int):
        session:Session = self.sessionmaker()
        session.query(Verse).filter_by(verse_id=verse_id).update({"status":False})

if __name__ == "__main__":
    db = XBibleDB(DB_PATH)
    verse_db = db.find_all(120, 100, 400)
    elements:List[XVerseWS] = [XVerseWS(item.verse, item.text) for item in verse_db]
    make_interior(elements)
    for item in verse_db:
        db.update_status(item.verse_id)