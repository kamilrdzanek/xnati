from hashlib import md5
from typing import Tuple, List
from copy import deepcopy
from PIL import Image, ImageDraw, ImageFont
from os.path import join
from datetime import datetime
import random
import math

MAX_X = 20
MAX_Y = 20
ALLOW_BACKWARDS_WORDS = False

COLOR_BLACK = (0,0,0)
COLOR_GRAY = (128,128,128)

PUZZLE_NAME_FORMAT = "PUZZLE {number}"

DIR_INPUT_FONT_RES = '/home/rydzyk/Dokumenty/Projects/vsRepo/xnati/res/fonts/tillana-extrabold.ttf'
DIR_INPUT_WS_PAGES_RES = '/home/rydzyk/Dokumenty/Projects/vsRepo/xnati/res/word_search'

DIR_RESULT_WS = "/home/rydzyk/Dokumenty/Projects/vsRepo/xnati/result/word_search"

TEMPLATE_ONE_PAGE_WS = join(DIR_INPUT_WS_PAGES_RES, 'Template_standard.jpg')
TEMPLATE_TWO_PAGE_WS_1 = join(DIR_INPUT_WS_PAGES_RES, 'Template_extension_1.jpg')
TEMPLATE_TWO_PAGE_WS_2 = join(DIR_INPUT_WS_PAGES_RES, 'Template_extension_2.jpg')
TEMPLATE_ANSWER_PAGE_WS_1 = join(DIR_INPUT_WS_PAGES_RES, 'Template_answer_1.jpg')
TEMPLATE_ANSWER_PAGE_WS_2 = join(DIR_INPUT_WS_PAGES_RES, 'Template_answer_2.jpg')
TEMPLATE_ANSWER_PAGE_WS_3 = join(DIR_INPUT_WS_PAGES_RES, 'Template_answer_3.jpg')
TEMPLATE_ANSWER_PAGE_WS_4 = join(DIR_INPUT_WS_PAGES_RES, 'Template_answer_4.jpg')
TEMPLATE_ANSWER_PAGE_WS_5 = join(DIR_INPUT_WS_PAGES_RES, 'Template_answer_5.jpg')
TEMPLATE_ANSWER_PAGE_WS_6 = join(DIR_INPUT_WS_PAGES_RES, 'Template_answer_6.jpg')
TEMPLATE_ANSWER_PAGES_WS = [
    TEMPLATE_ANSWER_PAGE_WS_1, TEMPLATE_ANSWER_PAGE_WS_2, TEMPLATE_ANSWER_PAGE_WS_3,
    TEMPLATE_ANSWER_PAGE_WS_4, TEMPLATE_ANSWER_PAGE_WS_5, TEMPLATE_ANSWER_PAGE_WS_6
    ]
TEMPLATE_VOCABULARY_PAGE_WS = join(DIR_INPUT_WS_PAGES_RES, 'Template_vocabulary.jpg')

FONT_SIZE_20 = ImageFont.truetype(DIR_INPUT_FONT_RES, 20)
FONT_SIZE_28 = ImageFont.truetype(DIR_INPUT_FONT_RES, 28)
FONT_SIZE_30 = ImageFont.truetype(DIR_INPUT_FONT_RES, 30)
FONT_SIZE_34 = ImageFont.truetype(DIR_INPUT_FONT_RES, 34)
FONT_SIZE_36 = ImageFont.truetype(DIR_INPUT_FONT_RES, 36)
FONT_SIZE_40 = ImageFont.truetype(DIR_INPUT_FONT_RES, 40)
FONT_SIZE_45 = ImageFont.truetype(DIR_INPUT_FONT_RES, 45)
FONT_SIZE_50 = ImageFont.truetype(DIR_INPUT_FONT_RES, 50)
FONT_SIZE_55 = ImageFont.truetype(DIR_INPUT_FONT_RES, 55)
FONT_SIZE_80 = ImageFont.truetype(DIR_INPUT_FONT_RES, 80)
FONT_SIZE_100 = ImageFont.truetype(DIR_INPUT_FONT_RES, 100)

class XWS():
    def __init__(self,
                 ws_number:int,
                 grid:List[List[str]],
                 grid_answer:List[List[str]],
                 input_data:List[str]) -> None:
        self.ws_number = ws_number
        self.grid = grid
        self.grid_answer = grid_answer
        self.input_data = input_data

def get_alphabet_from_words(input_data:List[str]):
    result = []
    for word in input_data:
        for char in word.upper():
            if char not in result:
                result.append(char)
    return result

def generate_ws(words:List[str], alphabet:List[str]):
    grid = [['*']*MAX_X for _ in range(MAX_Y)]
    for word in words:
        _place_word(grid, word)
    grid_answer = deepcopy(grid)
    for irow in range(MAX_X):
        for icol in range(MAX_Y):
            if grid[irow][icol] == '*':
                grid[irow][icol] = random.choice(alphabet)
    return grid, grid_answer

def make_interior(input_data:List[str],
                  count_of_words_per_ws:int=14,
                  ws_count:int=120,
                  alphabet:List[str]=None):
    alphabet_value = alphabet if alphabet is not None else get_alphabet_from_words(input_data)
    puzzle_number = 1
    puzzles = []
    index = 0
    while len(puzzles) < ws_count:
        skip_value = count_of_words_per_ws * index
        part_input_data = input_data[skip_value:]
        part_input_data = part_input_data[:count_of_words_per_ws]
        part_input_data = [str(item).upper() for item in part_input_data]
        if (len(part_input_data)==0):
            break
        grid, grid_answer = generate_ws(part_input_data, alphabet_value)
        item = XWS(puzzle_number, grid, grid_answer, part_input_data)
        puzzles.append(item)
        print("Generate word search: {number}".format(number=puzzle_number))
        index += 1
        puzzle_number = index + 1
    hash_value = md5(str(datetime.now()).encode('utf-8')).hexdigest() 
    one_page_imgs = make_ws_one_page(puzzles)
    location_to_save = join(DIR_RESULT_WS, "WS_ONE_%s.pdf" % hash_value[:6])
    img_puzzle:Image = deepcopy(one_page_imgs[0])
    img_puzzle.save(location_to_save, save_all=True, append_images=one_page_imgs[1:])
    two_page_imgs = make_ws_two_page(puzzles)
    location_to_save = join(DIR_RESULT_WS, "WS_TWO_%s.pdf" % hash_value[:6])
    img_puzzle:Image = deepcopy(two_page_imgs[0])
    img_puzzle.save(location_to_save, save_all=True, append_images=two_page_imgs[1:])
    solution_page_imgs = make_ws_solution_page(puzzles)
    location_to_save = join(DIR_RESULT_WS, "WS_SOLUTION_%s.pdf" % hash_value[:6])
    img_puzzle:Image = deepcopy(solution_page_imgs[0])
    img_puzzle.save(location_to_save, save_all=True, append_images=solution_page_imgs[1:])

def make_ws_one_page(puzzles:List[XWS]):
    images = []
    for index, ws_puzzle in enumerate(puzzles):
        puzzle_name = PUZZLE_NAME_FORMAT.format(number=ws_puzzle.ws_number)
        img = Image.open(TEMPLATE_ONE_PAGE_WS)
        img_editable = ImageDraw.Draw(img)
        '''Add title'''
        title_positions = (1000, 240)
        img_editable.text(title_positions, puzzle_name, fill=COLOR_BLACK, font=FONT_SIZE_100)
        '''Add chars'''
        char_start_position = (350, 450)
        for row in range(len(ws_puzzle.grid)):
            for col in range(len(ws_puzzle.grid[row])):
                img_editable.text((char_start_position[0] + col * 95, char_start_position[1] + row * 80), \
                ws_puzzle.grid[row][col], fill=COLOR_BLACK, font=FONT_SIZE_55)
        """Add words"""
        x_one_word_value = 320
        x_two_word_value = 1300
        y_word_value = 2220
        for index, element in enumerate(ws_puzzle.input_data):
            if index % 2 == 0:
                img_editable.text((x_one_word_value, y_word_value), element, \
                    fill=COLOR_BLACK, font=FONT_SIZE_50)
            else:
                img_editable.text((x_two_word_value, y_word_value), element, \
                    fill=COLOR_BLACK, font=FONT_SIZE_50)
                y_word_value = y_word_value + 100
        print("Maked standard image puzzle number: {number}".format(number=ws_puzzle.ws_number))
        # location_to_save = join(DIR_RESULT_WS, "PUZZLE_A_%s.jpg" % ws_puzzle.ws_number)
        # img.save(location_to_save)
        images.append(img)
    return images

def make_ws_two_page(puzzles:List[XWS]):
    first_title_position = (1000, 240)
    first_char_position = (880, 360)
    first_word_position = (350, 360)
    second_title_position = (1000, 1660)
    second_char_position = (300, 1780)
    second_word_position = (1800, 1780)
    NUMBER_OF_IMAGES = 2
    number_of_epoch = math.ceil(float(len(puzzles)) / NUMBER_OF_IMAGES)
    images = []
    def make_extension_page(title_position:Tuple[int, int], 
                            char_position:Tuple[int, int], 
                            words_start_position:Tuple[int, int], 
                            puzzle:XWS):
        puzzle_name = PUZZLE_NAME_FORMAT.format(number=puzzle.ws_number)
        img_editable.text(title_position, puzzle_name, \
            fill=COLOR_BLACK, font=FONT_SIZE_80)
        for row in range(len(puzzle.grid)):
            for col in range(len(puzzle.grid[row])):
                img_editable.text((char_position[0] + col * 70, char_position[1] + row * 59), \
                    puzzle.grid[row][col], fill=COLOR_BLACK, font=FONT_SIZE_45)
        y_word_value = words_start_position[1]
        for word in puzzle.input_data:
            img_editable.text((words_start_position[0], y_word_value), \
                word, fill=COLOR_BLACK, font=FONT_SIZE_30)
            y_word_value = y_word_value + 82
    for index in range(number_of_epoch):
        ws_part = puzzles[index * NUMBER_OF_IMAGES:]
        ws_part = ws_part[:NUMBER_OF_IMAGES]
        ws_puzzle_first = ws_part[0]
        ws_puzzle_second = None
        template = TEMPLATE_TWO_PAGE_WS_1
        if (len(ws_part)>1):
            ws_puzzle_second = ws_part[1]
            template = TEMPLATE_TWO_PAGE_WS_2
        img = Image.open(template)
        img_editable = ImageDraw.Draw(img)
        make_extension_page(first_title_position, first_char_position, first_word_position, ws_puzzle_first)
        if (ws_puzzle_second is not None):
            make_extension_page(second_title_position, second_char_position, second_word_position, ws_puzzle_second)
        print("Maked extension image puzzle number: {number}".format(number=ws_puzzle_first.ws_number))
        # location_to_save = join(DIR_RESULT_WS, "PUZZLE_B_%s.jpg" % ws_puzzle_first.ws_number)
        # img.save(location_to_save)
        images.append(img)
    return images

def make_ws_solution_page(puzzles:List[XWS]):
    NUMBER_OF_IMAGES = 6
    number_of_epoch = math.ceil(float(len(puzzles)) / NUMBER_OF_IMAGES)
    images = []
    title_position_one = (620, 240)
    title_position_two = (1700, 1180)
    y_title_three = 2110
    char_position_one = (345, 300)
    char_position_two = (1425, 1240)
    y_char_three = 2175
    def get_positions(index:int):
        if index == 0:
            return (title_position_one[0], title_position_one[1], char_position_one[0], char_position_one[1])
        elif index == 1:
            return (title_position_two[0], title_position_one[1], char_position_two[0], char_position_one[1])
        elif index == 2:
            return (title_position_one[0], title_position_two[1], char_position_one[0], char_position_two[1])
        elif index == 3:
            return (title_position_two[0], title_position_two[1], char_position_two[0], char_position_two[1])
        elif index == 4:
            return (title_position_one[0], y_title_three, char_position_one[0], y_char_three)
        else:
            return (title_position_two[0], y_title_three, char_position_two[0], y_char_three)
    for index in range(number_of_epoch):
        ws_part = puzzles[index * NUMBER_OF_IMAGES:]
        ws_part = ws_part[:NUMBER_OF_IMAGES]
        template = TEMPLATE_ANSWER_PAGES_WS[len(ws_part)-1]
        img = Image.open(template)
        img_editable = ImageDraw.Draw(img)
        for index, puzzle in enumerate(ws_part):
            x_title_value, y_title_value, x_char_value, y_char_value = get_positions(index)
            puzzle_name = PUZZLE_NAME_FORMAT.format(number=puzzle.ws_number)
            img_editable.text((x_title_value, y_title_value), puzzle_name, fill=COLOR_BLACK, font=FONT_SIZE_40)
            for row in range(len(puzzle.grid)):
                for col in range(len(puzzle.grid[row])):
                    char_answer = puzzle.grid_answer[row][col]
                    color_char = (char_answer != '*' and COLOR_BLACK or COLOR_GRAY)
                    font = (char_answer != '*' and FONT_SIZE_36 or FONT_SIZE_28)
                    img_editable.text((x_char_value + col * 40, y_char_value + row * 38), \
                        puzzle.grid[row][col], fill=color_char, font=font)
        print("Maked answer image puzzle number: {number}".format(number=ws_part[0].ws_number))
        # location_to_save = join(DIR_RESULT_WS, "PUZZLE_Q_%s.jpg" % ws_part[0].ws_number)
        # img.save(location_to_save)
        images.append(img)
    return images

def _place_word(grid:List[List[str]], word:str):
    """Place word randomly in the grid and return True, if possible."""
    # Left, down, and the diagonals.
    dxdy_choices = [(0,1), (1,0), (1,1), (1,-1)]
    random.shuffle(dxdy_choices)
    for (dx, dy) in dxdy_choices:
        if  ALLOW_BACKWARDS_WORDS and random.choice([True, False]):
                # If backwards words are allowed, simply reverse word.
                word = word[::-1]
        # Work out the minimum and maximum column and row indexes, given
        # the word length.
        n = len(word)
        colmin = 0
        colmax = MAX_Y - n if dx else MAX_Y - 1
        rowmin = 0 if dy >= 0 else n - 1
        rowmax = MAX_X - n if dy >= 0 else MAX_X - 1
        if colmax - colmin < 0 or rowmax - rowmin < 0:
            # No possible place for the word in this orientation.
            continue
        # Build a list of candidate locations for the word.
        candidates = []
        for irow in range(rowmin, rowmax+1):
            for icol in range(colmin, colmax+1):
                if _test_candidate(grid, irow, icol, dx, dy, word):
                    candidates.append((irow, icol))
        # If we don't have any candidates, try the next orientation.
        if not candidates:
            continue
        # Pick a random candidate location and place the word in this
        # orientation.
        loc = irow, icol = random.choice(candidates)
        for j in range(n):
            grid[irow][icol] = word[j]
            irow += dy
            icol += dx
        # We're done: no need to try any more orientations.
        break
    else:
        # If we're here, it's because we tried all orientations but
        # couldn't find anywhere to place the word. Oh dear.
        return None
    # print(word, loc, (dx, dy))
    return grid

def _test_candidate(grid:List[List[str]], irow, icol, dx, dy, word):
    """Test the candidate location (icol, irow) for word in orientation
        dx, dy)."""
    for j in range(len(word)):
        if grid[irow][icol] not in ('*', word[j]):
            return False
        irow += dy
        icol += dx
    return True

if __name__ == "__main__":
    input_data = ['Testabcdefgh11' for _ in range(200)]
    make_interior(input_data)