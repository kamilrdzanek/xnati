from argparse import ArgumentError
import random
from typing import Tuple

from pyrsistent import thaw

class XNumberFill():
    def __init__(self, size:Tuple[int, int]) -> None:
        self.size_x = size[0]
        self.size_y = size[1]
        self.grid = {}

def make_grid(size:Tuple[int, int], field_disable:float=0.1):
    if (size[0] < 10 or size[1] < 10):
        raise ArgumentError("Size is not correct")
    if (size[0] % 2 != 0 or size[1] % 2 != 0):
        raise ArgumentError("Size is not % 2 == 0")
    grid_part1 = {}
    grid_part2 = {}
    max_y = int(size[1] / 2)
    for y in range(max_y):
        for x in range(size[0]):
            rand_number = random.uniform(0,1)
            grid_part1[(x,y)] = '#' if rand_number < field_disable else '_'
            grid_part2[(size[0] - x - 1, max_y - y - 1)] = '#' if rand_number < field_disable else '_'
    grid_empty = grid_part1
    for y in range(max_y):
        for x in range(size[0]):
            grid_empty[(x,max_y + y)] = grid_part2[(x,y)]
    grid_text = ''
    for y in range(size[1]):
        for x in range(size[0]):
            grid_text += str(grid_empty[(x,y)])
        grid_text += '\n'
    print(grid_text)

def generate_number_fill(size:Tuple[int, int], field_disable:float=0.2):
    grid = XNumberFill(size)
    for y in range(grid.size_y):
        for x in range(grid.size_x):
            rand_number = random.uniform(0,1)
            if (rand_number < field_disable):
                if (x == 1):
                    if (grid.grid[(x-1,y)] != -1):
                        grid.grid[(x,y)] = random.randint(1,9)
                    else:
                        grid.grid[(x,y)] = -1
                elif (x > 1):
                    if (grid.grid[(x-2,y)] == -1 and grid.grid[(x-1,y)] != -1):
                        grid.grid[(x,y)] = random.randint(1,9)
                    else:
                        grid.grid[(x,y)] = -1
                else:
                    grid.grid[(x,y)] = -1
            else:
                grid.grid[(x,y)] = random.randint(1,9)
    grid_text = ''
    for y in range(grid.size_y):
        for x in range(grid.size_x):
            if (y == 1):
                if (grid.grid[(x,y-1)] != -1):
                    grid.grid[(x,y)] = random.randint(1,9)
            elif (y > 1):
                if (grid.grid[(x,y-1)] == -1 and grid.grid[(x,y-1)] != -1):
                    grid.grid[(x,y)] = random.randint(1,9)
            grid_text += '#' if grid.grid[(x,y)] == -1 else str(grid.grid[(x,y)])
        grid_text += '\n'
    print (grid_text)
    
if __name__ == "__main__":
    make_grid((20,20))