from selenium import webdriver
from NScraperDB import NScraperDB
from typing import Dict, Tuple

CHROME_DRIVER_PATH = "/home/rydzyk/Dokumenty/Projects/vsRepo/nati/res/driver/chromedriver"
URL = "https://1sudoku.com/sudoku/{level}"
DATABASE_PATH = "/home/rydzyk/Dokumenty/Projects/vsRepo/xnati/database/Sudoku9.sqllite"

LEVEL_EASY = ("easy", 0)
LEVEL_MEDIUM = ("medium", 1)
LEVEL_HARD = ("hard", 2)
LEVEL_EXPERT = ("expert", 3)
LEVEL_EVIL = ("evil", 4)
LEVELS = [LEVEL_EASY, LEVEL_MEDIUM, LEVEL_HARD, LEVEL_EXPERT, LEVEL_EVIL]

TRANSFORM_ARRAY = {
    0:8, 1:7, 2:6, 3:5, 4:4, 5:3, 6:2, 7:1, 8:0
}

ITERATION_NUMBER = 220

CELS_NUMBER = 9

class NScraperSudoku9():
    def __init__(self) -> None:
        self.database = NScraperDB(DATABASE_PATH)

    def open_browser(self):
        if not hasattr(self, 'web_browser') or self.web_browser is None:
            options = webdriver.ChromeOptions()
            options.add_argument('headless')
            options.add_argument('window-size=1920x1080')
            options.add_argument("disable-gpu")
            self.web_browser = webdriver.Chrome(CHROME_DRIVER_PATH, chrome_options=options)

    def scrappy(self, level:Tuple[str, int]):
        self.open_browser()
        for index in range(ITERATION_NUMBER):
            self.scrappy_one_puzzle(level)
            print("Scrappy: {level} number {number}".format(level=level[0], number=index))

    def scrappy_one_puzzle(self, level:Tuple[str, int]):
        level_name, level_number = level
        url_level = URL.format(level=level_name)
        self.web_browser.get(url_level)
        self.web_browser.implicitly_wait(2)
        try:
            accept = self.web_browser.find_element_by_id("consent_accepter");
            if accept is not None:
                accept.click()
                self.web_browser.get(url_level)
                self.web_browser.implicitly_wait(2)
        except:
            pass
        self.web_browser.find_element_by_class_name("icofont-key").click()
        values = {}
        solution = {}
        index = 0
        for row in range(CELS_NUMBER):
            for col in range(CELS_NUMBER):
                element = self.web_browser.find_element_by_id('c_{number}'.format(number=index))
                value = element.get_attribute('v')
                class_att = element.get_attribute("class")
                if 'fixe' in class_att:
                    values[(row,col)] = value
                if value is None:
                    value = ''
                solution[(row,col)] = value
                index += 1 
        self.database.insert(url_level, -1, level_number, values, solution)
        values_1, solution_1 = self._make_transform_puzzle(values, solution, True, False)
        self.database.insert(url_level, -11, level_number, values_1, solution_1)
        values_2, solution_2 = self._make_transform_puzzle(values, solution, False, True)
        self.database.insert(url_level, -111, level_number, values_2, solution_2)
        values_3, solution_3 = self._make_transform_puzzle(values, solution, True, True)
        self.database.insert(url_level, -1111, level_number, values_3, solution_3)

    def _make_transform_puzzle(self, grid:dict, grid_solution:dict, row_bool:bool, col_bool:bool):
        grid_transform = {}
        grid_solution_transform = {}
        if row_bool == True and col_bool == False:
            for row in range(CELS_NUMBER):
                for col in range(CELS_NUMBER):
                    if ((row, col) in grid):
                        grid_transform[(TRANSFORM_ARRAY[row], col)] = grid[(row, col)]
                    grid_solution_transform[(TRANSFORM_ARRAY[row], col)] = grid_solution[(row, col)]
        if row_bool == False and col_bool == True:
            for row in range(CELS_NUMBER):
                for col in range(CELS_NUMBER):
                    if ((row, col) in grid):
                        grid_transform[(row, TRANSFORM_ARRAY[col])] = grid[(row, col)]
                    grid_solution_transform[(row, TRANSFORM_ARRAY[col])] = grid_solution[(row, col)]
        if row_bool == True and col_bool == True:
            for row in range(CELS_NUMBER):
                for col in range(CELS_NUMBER):
                    if ((row, col) in grid):
                        grid_transform[(TRANSFORM_ARRAY[row], TRANSFORM_ARRAY[col])] = grid[(row, col)]
                    grid_solution_transform[(TRANSFORM_ARRAY[row], TRANSFORM_ARRAY[col])] = grid_solution[(row, col)]
        return grid_transform, grid_solution_transform


if __name__ == "__main__":
    scrap = NScraperSudoku9()
    # scrap.scrappy(LEVEL_EASY)
    # scrap.scrappy(LEVEL_MEDIUM)
    scrap.scrappy(LEVEL_HARD)
    # scrap.scrappy(LEVEL_EXPERT)

    print("Done!")
