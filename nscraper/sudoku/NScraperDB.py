from sqlite3 import connect as open_sqlite
from typing import List, Dict, Tuple
from hashlib import md5
import json

TUPLE_FORMAT = '{t1}||{t2}'
KEY_MD5_HASH = 'md5_hash'
KEY_RANK = 'rank'
KEY_GRID = 'grid'
KEY_GRID_SOLUTION = 'grid_solution'
SPECIAL_CHAR = '||'

SQL_QUERY_CREATE_SUDOKU_TABLE = \
    "CREATE TABLE IF NOT EXISTS SUDOKU \
    ( \
        SUDOKUID INTEGER PRIMARY KEY AUTOINCREMENT, \
        HASH TEXT NOT NULL, \
        RANK INTEGER NOT NULL, \
        GRID TEXT NOT NULL, \
        GRID_SOLUTION TEXT NOT NULL, \
        STATUS INTEGER DEFAULT 0, \
        UNIQUE(HASH) \
    )"

SQL_QUERY_INSERT_SUDOKU = \
    'INSERT OR REPLACE INTO SUDOKU(HASH, RANK, GRID, GRID_SOLUTION) VALUES(?, ?, ?, ?)'

SQL_QUERY_SELECT_SUDOKU_BY_GRID = \
    'SELECT HASH, RANK, GRID, GRID_SOLUTION FROM SUDOKU WHERE GRID = ?'

SQL_QUERY_CREATE_SETTING_TABLE = \
    "CREATE TABLE IF NOT EXISTS SETTING \
    ( \
        SETTINGID INTEGER PRIMARY KEY AUTOINCREMENT, \
        URL TEXT NOT NULL, \
        RANK INTEGER NOT NULL, \
        NUMBER INTEGER NOT NULL, \
        HASH TEXT NOT NULL, \
        UNIQUE(HASH) \
    )"

SQL_QUERY_INSERT_SETTING = \
    'INSERT OR REPLACE INTO SETTING (URL, RANK, NUMBER, HASH) VALUES(?, ?, ?, ?)'

SQL_QUERY_SELECT_SETTING_MAX_NUMBER_BY_RANK = \
    ''

def calculate_md5(params:List):
    str_params = [str(item) for item in params]
    value_to_md5 = '|'.join(str_params)
    return md5(value_to_md5.encode('utf-8')).hexdigest() 

def to_json(value_dict:Dict[Tuple[int, int],int]) -> str:
    dict_to_json = {}
    for key, value in value_dict.items():
        new_key = TUPLE_FORMAT.format(t1=key[0],t2=key[1])
        dict_to_json[new_key] = value
    return json.dumps(dict_to_json, indent = 4)

class NScraperDB():
    def __init__(self, path:str):
        self.path = path
        self.sql_driver = open_sqlite(path)
        self._create_structure_if_not_exists()

    def insert(self, url:str, number:int, rank:int, grid:dict, grid_solution:dict):
        grid_json = to_json(grid)
        grid_solution_json = to_json(grid_solution)
        params = [rank, grid_json, grid_solution_json]
        params.insert(0, calculate_md5(params))
        self._execute_sql_query(SQL_QUERY_INSERT_SUDOKU, params)
        data = self._select_sql_query(SQL_QUERY_SELECT_SUDOKU_BY_GRID, [grid_json])
        if len(data) > 0 and len(data[0]) > 0:
            md5_value = data[0][0]
            setting_params = [url, rank, number, md5_value]
            self._execute_sql_query(SQL_QUERY_INSERT_SETTING, setting_params)

    def find_max_setting_number(self, rank:int):
        data = self._select_sql_query(SQL_QUERY_SELECT_SETTING_MAX_NUMBER_BY_RANK, [rank])
        if len(data) > 0 and len(data[0]) > 0:
            max_number = data[0][0]
            return max_number
        return -1
    
    def _create_structure_if_not_exists(self):
        cursor = self.sql_driver.cursor()
        cursor.executescript(SQL_QUERY_CREATE_SUDOKU_TABLE)
        cursor.executescript(SQL_QUERY_CREATE_SETTING_TABLE)
        self.sql_driver.commit()
    
    def _execute_sql_query(self, sql_query:str, params:List):
        cursor = self.sql_driver.cursor()
        cursor.execute(sql_query, params)
        self.sql_driver.commit()

    def _select_sql_query(self, sql_query:str, params:List):
        cursor = self.sql_driver.cursor()
        cursor.execute(sql_query, params)
        return cursor.fetchall()
