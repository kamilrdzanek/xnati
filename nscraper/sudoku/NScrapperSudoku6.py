from logging import exception
from readline import redisplay
from selenium import webdriver
from NScraperDB import NScraperDB
from typing import Dict, Tuple
from time import sleep

CHROME_DRIVER_PATH = "/home/rydzyk/Dokumenty/Projects/vsRepo/nati/res/driver/chromedriver"
URL = "https://sudokus.pl/6x6/"
DATABASE_PATH = "/home/rydzyk/Dokumenty/Projects/vsRepo/xnati/database/Sudoku6.sqllite"

LEVEL_EASY = ("easy", 0, "beginner")
LEVEL_MEDIUM = ("medium", 1, "easy")
LEVEL_HARD = ("hard", 2, "medium")
LEVEL_EXPERT = ("expert", 3, "hard")
LEVELS = [LEVEL_EASY, LEVEL_MEDIUM, LEVEL_HARD, LEVEL_EXPERT]

TRANSFORM_ARRAY = {
    0:8, 1:7, 2:6, 3:5, 4:4, 5:3, 6:2, 7:1, 8:0
}

#Expert
#
ITERATION_NUMBER = 1000

CELLS_NUMBER = 6

class NScraperSudoku6():
    def __init__(self) -> None:
        self.database = NScraperDB(DATABASE_PATH)

    def open_browser(self):
        if not hasattr(self, 'web_browser') or self.web_browser is None:
            options = webdriver.ChromeOptions()
            options.add_argument('headless')
            options.add_argument('window-size=1920x1080')
            options.add_argument("disable-gpu")
            self.web_browser = webdriver.Chrome(CHROME_DRIVER_PATH, chrome_options=options)

    def scrappy(self, level:Tuple[str, int, str]):
        self.open_browser()
        self.web_browser.get(URL)
        self.web_browser.implicitly_wait(2)
        for index in range(ITERATION_NUMBER):
            self.scrappy_one_puzzle(level)
            print("Scrappy: {level} number {number}".format(level=level[0], number=index))

    def scrappy_one_puzzle(self, level:Tuple[str, int]):
        name, level_number, level_text = level
        new_button = self.web_browser.find_element_by_id("new-game")
        new_button.click()
        sleep(1)
        xpath = "//button[@type='button' and @data-hash='{level}']".format(level=level_text)
        level_button = self.web_browser.find_element_by_xpath(xpath)
        level_button.click()
        sleep(1)
        solve_button = self.web_browser.find_element_by_id("solve-game")
        solve_button.click()
        sleep(1)
        all_solve_button = self.web_browser.find_element_by_id("solve-game-all")
        all_solve_button.click()
        sleep(1)
        cells_tmp = self.web_browser.find_elements_by_xpath("//input")
        cells = []
        for cell in cells_tmp:
            try:
                class_att = cell.get_attribute("class")
                if 'cell' not in class_att:
                    continue
                cells.append(cell)
            except:
                continue
        values = {}
        solution = {}
        index = -1
        for row in range(CELLS_NUMBER):
            for col in range(CELLS_NUMBER):
                index += 1
                element = cells[index]
                class_att = ''
                try:
                    class_att = element.get_attribute("class")
                except:
                    continue
                if 'cell' not in class_att:
                    continue
                value_str = element.get_attribute('value')
                if value_str not in ['0','1','2','3','4','5','6','7','8','9']:
                    continue
                value = int(value_str)
                if 'fixed' in class_att:
                    values[(row,col)] = value
                solution[(row,col)] = value
        self.database.insert(URL, -1, level_number, values, solution)
        values_1, solution_1 = self._make_transform_puzzle(values, solution, True, False)
        self.database.insert(URL, -11, level_number, values_1, solution_1)
        values_2, solution_2 = self._make_transform_puzzle(values, solution, False, True)
        self.database.insert(URL, -111, level_number, values_2, solution_2)
        values_3, solution_3 = self._make_transform_puzzle(values, solution, True, True)
        self.database.insert(URL, -1111, level_number, values_3, solution_3)

    def _make_transform_puzzle(self, grid:dict, grid_solution:dict, row_bool:bool, col_bool:bool):
        grid_transform = {}
        grid_solution_transform = {}
        if row_bool == True and col_bool == False:
            for key, value in grid_solution.items():
                key_row, key_col = key
                new_key = (TRANSFORM_ARRAY[key_row], key_col)
                grid_solution_transform[new_key] = value
                if key in grid:
                    grid_transform[new_key] = value
        if row_bool == False and col_bool == True:
            for key, value in grid_solution.items():
                key_row, key_col = key
                new_key = (key_row, TRANSFORM_ARRAY[key_col])
                grid_solution_transform[new_key] = value
                if key in grid:
                    grid_transform[new_key] = value
        if row_bool == True and col_bool == True:
            for key, value in grid_solution.items():
                key_row, key_col = key
                new_key = (TRANSFORM_ARRAY[key_row], TRANSFORM_ARRAY[key_col])
                grid_solution_transform[new_key] = value
                if key in grid:
                    grid_transform[new_key] = value
        return grid_transform, grid_solution_transform


if __name__ == "__main__":
    scrap = NScraperSudoku6()
    # scrap.scrappy(LEVEL_EASY)
    # scrap.scrappy(LEVEL_MEDIUM)
    # scrap.scrappy(LEVEL_HARD)
    # scrap.scrappy(LEVEL_EXPERT)