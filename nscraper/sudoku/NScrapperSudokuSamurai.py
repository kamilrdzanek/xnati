from selenium import webdriver
from selenium.webdriver.support.ui import Select
from NScraperDB import NScraperDB
from typing import Dict, Tuple
from time import sleep
import os

CHROME_DRIVER_PATH = "/home/rydzyk/Dokumenty/Projects/vsRepo/nati/res/driver/chromedriver"
URL = "https://www.samurai-sudoku.com/"
DATABASE_PATH = "/home/rydzyk/Dokumenty/Projects/vsRepo/xnati/database/SudokuSamurai.sqllite"
SAMURAI_SUDOKU_SELECTS = "/home/rydzyk/Dokumenty/Projects/vsRepo/xnati/database/SudokuSamurai.txt"

LEVEL_EASY = ("easy", 0, "Easy")
LEVEL_MEDIUM = ("medium", 1, "Moderate")
LEVEL_HARD = ("hard", 2, "Hard")
LEVEL_EXPERT = ("expert", 3, "Evil")
LEVEL_UNKNOWN = ("unknown", 4, "Fiendish")
LEVELS = [LEVEL_EASY, LEVEL_MEDIUM, LEVEL_HARD, LEVEL_EXPERT, LEVEL_UNKNOWN]

SUDOKU_SAMURAI_POSITION = [
    (1,1), (1,2), (1,3), (1,4), (1,5), (1,6), (1,7), (1,8), (1,9), 
    (1,13), (1,14), (1,15), (1,16), (1,17), (1,18), (1,19), (1,20), (1,21),
    (2,1), (2,2), (2,3), (2,4), (2,5), (2,6), (2,7), (2,8), (2,9), 
    (2,13), (2,14), (2,15), (2,16), (2,17), (2,18), (2,19), (2,20), (2,21),
    (3,1), (3,2), (3,3), (3,4), (3,5), (3,6), (3,7), (3,8), (3,9), 
    (3,13), (3,14), (3,15), (3,16), (3,17), (3,18), (3,19), (3,20), (3,21),
    (4,1), (4,2), (4,3), (4,4), (4,5), (4,6), (4,7), (4,8), (4,9), 
    (4,13), (4,14), (4,15), (4,16), (4,17), (4,18), (4,19), (4,20), (4,21),
    (5,1), (5,2), (5,3), (5,4), (5,5), (5,6), (5,7), (5,8), (5,9), 
    (5,13), (5,14), (5,15), (5,16), (5,17), (5,18), (5,19), (5,20), (5,21),
    (6,1), (6,2), (6,3), (6,4), (6,5), (6,6), (6,7), (6,8), (6,9), 
    (6,13), (6,14), (6,15), (6,16), (6,17), (6,18), (6,19), (6,20), (6,21),
    (7,1), (7,2), (7,3), (7,4), (7,5), (7,6), (7,7), (7,8), (7,9), (7,10), (7,11), 
    (7,12), (7,13), (7,14), (7,15), (7,16), (7,17), (7,18), (7,19), (7,20), (7,21),
    (8,1), (8,2), (8,3), (8,4), (8,5), (8,6), (8,7), (8,8), (8,9), (8,10), (8,11), 
    (8,12), (8,13), (8,14), (8,15), (8,16), (8,17), (8,18), (8,19), (8,20), (8,21),
    (9,1), (9,2), (9,3), (9,4), (9,5), (9,6), (9,7), (9,8), (9,9), (9,10), (9,11), 
    (9,12), (9,13), (9,14), (9,15), (9,16), (9,17), (9,18), (9,19), (9,20), (9,21),
    (10,7), (10,8), (10,9), (10,10), (10,11), (10,12), (10,13), (10,14), (10,15),
    (11,7), (11,8), (11,9), (11,10), (11,11), (11,12), (11,13), (11,14), (11,15),
    (12,7), (12,8), (12,9), (12,10), (12,11), (12,12), (12,13), (12,14), (12,15),
    (13,1), (13,2), (13,3), (13,4), (13,5), (13,6), (13,7), (13,8), (13,9), (13,10), (13,11), 
    (13,12), (13,13), (13,14), (13,15), (13,16), (13,17), (13,18), (13,19), (13,20), (13,21),
    (14,1), (14,2), (14,3), (14,4), (14,5), (14,6), (14,7), (14,8), (14,9), (14,10), (14,11), 
    (14,12), (14,13), (14,14), (14,15), (14,16), (14,17), (14,18), (14,19), (14,20), (14,21),
    (15,1), (15,2), (15,3), (15,4), (15,5), (15,6), (15,7), (15,8), (15,9), (15,10), (15,11), 
    (15,12), (15,13), (15,14), (15,15), (15,16), (15,17), (15,18), (15,19), (15,20), (15,21),
    (16,1), (16,2), (16,3), (16,4), (16,5), (16,6), (16,7), (16,8), (16,9), 
    (16,13), (16,14), (16,15), (16,16), (16,17), (16,18), (16,19), (16,20), (16,21),
    (17,1), (17,2), (17,3), (17,4), (17,5), (17,6), (17,7), (17,8), (17,9), 
    (17,13), (17,14), (17,15), (17,16), (17,17), (17,18), (17,19), (17,20), (17,21),
    (18,1), (18,2), (18,3), (18,4), (18,5), (18,6), (18,7), (18,8), (18,9), 
    (18,13), (18,14), (18,15), (18,16), (18,17), (18,18), (18,19), (18,20), (18,21),
    (19,1), (19,2), (19,3), (19,4), (19,5), (19,6), (19,7), (19,8), (19,9), 
    (19,13), (19,14), (19,15), (19,16), (19,17), (19,18), (19,19), (19,20), (19,21),
    (20,1), (20,2), (20,3), (20,4), (20,5), (20,6), (20,7), (20,8), (20,9), 
    (20,13), (20,14), (20,15), (20,16), (20,17), (20,18), (20,19), (20,20), (20,21),
    (21,1), (21,2), (21,3), (21,4), (21,5), (21,6), (21,7), (21,8), (21,9), 
    (21,13), (21,14), (21,15), (21,16), (21,17), (21,18), (21,19), (21,20), (21,21)]

TRANSFORM_ARRAY = {
    0:20, 1:19, 2:18, 3:17, 4:16, 5:15, 6:14, 7:13, 8:12, 9:11, 10:10, 
    11:9, 12:8, 13:7, 14:6, 15:5, 16:4, 17:3, 18:2, 19:1, 20:0
}

CELS_NUMBER = 22

class NScraperSudoku16():
    def __init__(self) -> None:
        self.database = NScraperDB(DATABASE_PATH)

    def open_browser(self):
        if not hasattr(self, 'web_browser') or self.web_browser is None:
            options = webdriver.ChromeOptions()
            options.add_argument('headless')
            options.add_argument('window-size=1920x1080')
            options.add_argument("disable-gpu")
            self.web_browser = webdriver.Chrome(CHROME_DRIVER_PATH, chrome_options=options)

    def scrappy(self):
        self.open_browser()
        self.web_browser.get(URL)
        ai_selector = self.web_browser.find_element_by_id("ai")
        # ai_selector.click()
        selector_values = []
        if os.path.isfile(SAMURAI_SUDOKU_SELECTS):
            with open(SAMURAI_SUDOKU_SELECTS, 'r') as f:
                selector_values = f.readlines()
        else:
            options = self.web_browser.find_elements_by_xpath('//option')
            for item in options:
                selector_values.append(item.text)
            with open(SAMURAI_SUDOKU_SELECTS, 'w') as f:
                for item in selector_values:
                    f.write(item + '\n')
        sudoku_select = Select(ai_selector)
        values = {}
        solutions = {}
        index_value = 0
        for text in selector_values:
            ai_selector.click()
            sudoku_select.select_by_visible_text(text.replace("\n", ""))
            self.web_browser.find_element_by_id("b9").click()
            sleep(6)
            cels = self.web_browser.find_elements_by_xpath('//input[@class="i"]')
            for index, element in enumerate(cels):
                value = element.get_attribute("value")
                id_att = element.get_attribute("id")
                style_att = element.get_attribute("style")
                position = id_att.split("i")
                if (len(position) == 2):
                    pos_x = int(position[0])
                    pos_y = int(position[1])
                    solutions[(pos_x, pos_y)] = int(value)
                    if ('rgb(0, 0, 0)' in style_att):
                        values[(pos_x, pos_y)] = int(value)
            level_number = 0
            for name, rank, text_level in LEVELS:
                if text_level in text:
                    level_number = rank
                    break
            self.database.insert(URL, index_value, level_number, values, solutions)
            values_1, solution_1 = self._make_transform_puzzle(values, solutions, True, False)
            index_value_2 = 0 - index_value
            self.database.insert(URL, index_value_2, level_number, values_1, solution_1)
            values_2, solution_2 = self._make_transform_puzzle(values, solutions, False, True)
            self.database.insert(URL, index_value_2 * 1000, level_number, values_2, solution_2)
            values_3, solution_3 = self._make_transform_puzzle(values, solutions, True, True)
            self.database.insert(URL, index_value_2 * 1000000, level_number, values_3, solution_3)
            print("Scrapped samurai: {number}".format(number=index_value))
            index_value = index_value + 1

    def _make_transform_puzzle(self, grid:dict, grid_solution:dict, row_bool:bool, col_bool:bool):
        grid_transform = {}
        grid_solution_transform = {}
        if row_bool == True and col_bool == False:
            for key, value in grid_solution.items():
                key_row, key_col = key
                new_key = (TRANSFORM_ARRAY[key_row], key_col)
                grid_solution_transform[new_key] = value
                if key in grid:
                    grid_transform[new_key] = value
        if row_bool == False and col_bool == True:
            for key, value in grid_solution.items():
                key_row, key_col = key
                new_key = (key_row, TRANSFORM_ARRAY[key_col])
                grid_solution_transform[new_key] = value
                if key in grid:
                    grid_transform[new_key] = value
        if row_bool == True and col_bool == True:
            for key, value in grid_solution.items():
                key_row, key_col = key
                new_key = (TRANSFORM_ARRAY[key_row], TRANSFORM_ARRAY[key_col])
                grid_solution_transform[new_key] = value
                if key in grid:
                    grid_transform[new_key] = value
        return grid_transform, grid_solution_transform


if __name__ == "__main__":
    scrap = NScraperSudoku16()
    scrap.scrappy()