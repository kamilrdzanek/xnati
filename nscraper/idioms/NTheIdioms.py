from typing import List
import requests
from bs4 import BeautifulSoup

from NIdiomDB import Idiom, NIdiomDB

URL = "https://www.theidioms.com/list/page/%s/"
DATABASE_PATH = "/home/rydzyk/Dokumenty/Projects/vsRepo/xnati/database/Idiom.sqllite"
PAGES_NUMBER = 150

db = NIdiomDB(DATABASE_PATH)

def make_url(number:int):
    return URL % number

def save_all_idioms():
    settings = [item.key for item in db.find_all_setting()]
    for index in range(1, PAGES_NUMBER + 1):
        url = make_url(index)
        if url in settings:
            continue
        items = save_idioms_form_one_page(url)
        if items is not None and len(items) > 0:
            db.insert_collection(items)

def save_idioms_form_one_page(url) -> List[Idiom]:
    res = requests.get(url)
    soup = BeautifulSoup(res.content, "lxml")
    page_navi = soup.find('p', class_='pno')
    if (page_navi is None or len(page_navi.contents)<=0):
        return []
    table = soup.find('dl')
    idiom_text = idiom_mening = idiom_example = ''
    dt_bool = dd_bool = False
    items_to_insert = []
    if (table is not None and len(table.contents) > 0):
        for item in table.contents:
            if (item.name == 'dt'):
                idiom_text = item.text
                dt_bool = True
            if (item.name == 'dd' and len(item.contents)==2):
                value_1 = item.contents[0].text
                value_2 = item.contents[1].text
                values = [value_1, value_2]
                for element in values:
                    if (element is not None and len(element)>0 and 'Meaning:' in element):
                        idiom_mening = str(element).replace('Meaning:', '').rstrip().lstrip()
                    if (element is not None and len(element)>0 and 'Example:' in element):
                        idiom_example = str(element).replace('Example:', '').rstrip().lstrip()
                dd_bool = True
            if (dt_bool == True and dd_bool == True):
                obj = Idiom(idiom_text, idiom_mening, idiom_example)
                idiom_text = idiom_mening = idiom_example = ''
                items_to_insert.append(obj)
                print("Added idiom: %s" % obj.text)
                dt_bool = dd_bool = False
        db.insert_collection(items_to_insert)
    db.insert_setting(url)
    print("Added url: %s" % url)
    return items_to_insert

if __name__ == "__main__":
    save_all_idioms()
