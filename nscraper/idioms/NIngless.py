from typing import List
import requests
from bs4 import BeautifulSoup

from NIdiomDB import Idiom, NIdiomDB

URLS = ['https://www.ingless.pl/idiomy/angielski-polski/litera/a/',
        'https://www.ingless.pl/idiomy/angielski-polski/litera/b/',
        'https://www.ingless.pl/idiomy/angielski-polski/litera/c/',
        'https://www.ingless.pl/idiomy/angielski-polski/litera/d/',
        'https://www.ingless.pl/idiomy/angielski-polski/litera/e/',
        'https://www.ingless.pl/idiomy/angielski-polski/litera/f/',
        'https://www.ingless.pl/idiomy/angielski-polski/litera/g/',
        'https://www.ingless.pl/idiomy/angielski-polski/litera/h/',
        'https://www.ingless.pl/idiomy/angielski-polski/litera/i/',
        'https://www.ingless.pl/idiomy/angielski-polski/litera/j/',
        'https://www.ingless.pl/idiomy/angielski-polski/litera/k/',
        'https://www.ingless.pl/idiomy/angielski-polski/litera/l/',
        'https://www.ingless.pl/idiomy/angielski-polski/litera/m/',
        'https://www.ingless.pl/idiomy/angielski-polski/litera/n/',
        'https://www.ingless.pl/idiomy/angielski-polski/litera/o/',
        'https://www.ingless.pl/idiomy/angielski-polski/litera/p/',
        'https://www.ingless.pl/idiomy/angielski-polski/litera/q/',
        'https://www.ingless.pl/idiomy/angielski-polski/litera/r/',
        'https://www.ingless.pl/idiomy/angielski-polski/litera/s/',
        'https://www.ingless.pl/idiomy/angielski-polski/litera/t/',
        'https://www.ingless.pl/idiomy/angielski-polski/litera/u/',
        'https://www.ingless.pl/idiomy/angielski-polski/litera/v/',
        'https://www.ingless.pl/idiomy/angielski-polski/litera/w/',
        'https://www.ingless.pl/idiomy/angielski-polski/litera/x/',
        'https://www.ingless.pl/idiomy/angielski-polski/litera/y/',
        'https://www.ingless.pl/idiomy/angielski-polski/litera/z/']

DATABASE_PATH = "/home/rydzyk/Dokumenty/Projects/vsRepo/xnati/database/Idiom.sqllite"
BASE_URL = 'https://www.ingless.pl'
FORMT_BASE_URL = 'https://www.ingless.pl%s'

db = NIdiomDB(DATABASE_PATH)

def save_all_idioms():
    for url in URLS:
        if (db.contains_setting(url)):
            continue
        sub_pages:List[str] = get_sub_pages(url)
        save_all_idioms_from_sub_pages(sub_pages)
        db.insert_setting(url)

def get_sub_pages(url:str):
    res = requests.get(url)
    soup = BeautifulSoup(res.content, "lxml")
    sub_urls = soup.find_all('a', class_='idioms-page__text')
    result = [FORMT_BASE_URL % item['href'] for item in sub_urls]
    return result

def save_all_idioms_from_sub_pages(sub_pages:List[str]):
    for sub_page in sub_pages:
        url = sub_page if BASE_URL in sub_page else FORMT_BASE_URL % sub_page
        if (db.contains_setting(url)):
            continue
        res = requests.get(url)
        soup = BeautifulSoup(res.content, "lxml")
        sub_urls = soup.find_all('a', class_='idioms-page__text')
        result = [item['href'] for item in sub_urls]
        save_all_idioms_from_pages(result)
        db.insert_setting(url)

def save_all_idioms_from_pages(word_urls:List[str]):
    for word_url in word_urls:
        url = word_url if BASE_URL in word_url else FORMT_BASE_URL % word_url
        if (db.contains_setting(url)):
            continue
        res = requests.get(url)
        soup = BeautifulSoup(res.content, "lxml")
        idioms = soup.find_all('div', class_='idioms__content')
        if (len(idioms) > 1):
            eng_idiom = None
            pl_idiom = None
            for item in idioms[0].contents:
                if item.name == 'a':
                    eng_idiom = item.text
                    break
            for item in idioms[1].contents:
                if item.name == 'a':
                    pl_idiom = item.text
                    break
            if (eng_idiom is not None and pl_idiom is not None):
                item_to_insert = Idiom(eng_idiom, translate=pl_idiom)
                db.insert_collection([item_to_insert])
                db.insert_setting(url)


if __name__ == "__main__":
    save_all_idioms()