from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, String, Integer, Boolean
from sqlalchemy.orm import sessionmaker, Session
from sqlalchemy.sql.expression import func
from typing import List
from hashlib import md5

base = declarative_base()

def calculate_md5(params:List):
    str_params = [str(item) for item in params]
    value_to_md5 = '|'.join(str_params)
    return md5(value_to_md5.encode('utf-8')).hexdigest() 

class Idiom(base):
    __tablename__ = 'Idiom'

    idiom_id = Column('idiom_id', Integer, primary_key=True, autoincrement=True)
    text = Column('text', String, nullable=False, index=True, unique=True)
    hash_text = Column('hash_text', String, nullable=False)
    meaning = Column('meaning', String, nullable=True, default='')
    example = Column('example', String, nullable=True, default='')
    translate = Column('translate', String, nullable=True, default='')
    status = Column('status', Boolean, default=False)

    def __init__(self, idiom:str, meaning:str='', example:str='', translate:str='') -> None:
        self.text = idiom
        self.meaning = meaning
        self.example = example
        self.translate = translate
        self.hash_text = calculate_md5(idiom)

class Setting(base):
    __tablename__ = 'Setting'

    setting_id = Column('setting_id', Integer, primary_key=True, autoincrement=True)
    key = Column('key', String, nullable=False)
    value = Column('value', String, nullable=False)

    def __init__(self, key:str, value:str) -> None:
        self.key = key
        self.value = value

class NIdiomDB():
    def __init__(self, path:str) -> None:
        self.path = path
        self.engine = create_engine('sqlite:///%s' % path, echo=True)
        try:
            base.metadata.create_all(self.engine)
        except Exception:
            pass
        self.sessionmaker = sessionmaker(bind=self.engine,expire_on_commit=False)

    def insert_setting(self, url:str):
        session:Session = self.sessionmaker()
        element = session.query(Setting).filter_by(key=url).one_or_none()
        if element is not None:
            return
        session.add(Setting(url, 'TRUE'))
        session.commit()
        session.expunge_all()
        session.close()

    def insert_collection(self, idioms:List[Idiom]):
        session:Session = self.sessionmaker()
        for idiom in idioms:
            element = session.query(Idiom).filter_by(hash_text=idiom.hash_text).one_or_none()    
            if element is not None:
                return
            session.add(idiom)
        session.commit()
        session.expunge_all()
        session.close()

    def find_all_setting(self):
        session:Session = self.sessionmaker()
        elements = session.query(Setting).all()
        session.expunge_all()
        session.close()
        if elements is None or len(elements)<=0:
            return []
        return elements

    def find_all(self, limit:int=1000, random:bool=True):
        session:Session = self.sessionmaker()
        if random:
            elements = session.query(Idiom).filter_by(status=True).order_by(func.random()).limit(limit).all()
        else:
            elements = session.query(Idiom).filter_by(status=True).limit(limit).all()
        session.expunge_all()
        session.close()
        if elements is not None and len(elements) > 0:
            return elements

    def contains_setting(self, url:str):
        session:Session = self.sessionmaker()
        element = session.query(Setting).filter_by(key=url).one_or_none()
        return element is not None
