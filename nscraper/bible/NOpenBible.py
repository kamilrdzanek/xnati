from typing import List
from bs4 import BeautifulSoup
import pandas as pd
import requests
import re, time

from NBibleDB import NBibleDB, Verse

URL = "https://www.openbible.info/topics/%s"
URL_BASE = 'https://www.openbible.info'
DATABASE_PATH = "/home/rydzyk/Dokumenty/Projects/vsRepo/xnati/database/Bible.sqllite"
ALPHABET = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','Y','Z']

db = NBibleDB(DATABASE_PATH)

def make_url_by_alphabet():
    for leter in ALPHABET:
        yield URL % leter

def save_all_verses():
    settings:List[str] = [item.key for item in db.find_all_setting()]
    for url in make_url_by_alphabet():
        if (url in settings):
            continue
        save_all_verses_by_letter(url)
        db.insert_url_setting(url)
        db.clear_locket()
        print('Inserted letter url: %s' % url)

def save_all_verses_by_letter(url:str):
    settings:List[str] = [item.key for item in db.find_all_setting()]
    urls:List = get_all_topic_urls(url)
    for t,u in urls:
        if (u in settings):
            continue
        res = requests.get(u)
        soup = BeautifulSoup(res.content, "lxml")
        verses = [item for item in soup.find_all('div', class_='verse')]
        collection_to_insert = []
        for verse in verses:
            verse_value = ''
            text_value = ''
            for content in verse.contents:
                if (content.name == 'h3'):
                    array_value = re.split('\n|\t', str(content.text))
                    for value in array_value:
                        if (value is None or len(value) == 0):
                            continue
                        else:
                            verse_value = value
                            break
                if (content.name == 'p'):
                    text_value = str(content.text).replace('\n', '').replace('\t', '')
                if (verse_value != '' and text_value != ''):
                    verse_item = Verse(verse_value, text_value)
                    collection_to_insert.append(verse_item)
                    break
        db.insert_url_setting(u)
        db.clear_locket()
        db.insert_collection(collection_to_insert)
        # time.sleep(1)
        # for v in collection_to_insert:
        #     item = db.find_by_hash_text(v.hash_text)
        #     if (item is None):
        #         continue
        #     element = db.find_topic_by_topic(t, item.verse_id)
        #     if (element is None):
        #         db.insert_topic(t, item.verse_id)
        db.clear_locket()
        print('Inserted url: %s' % u)

def get_all_topic_urls(url:str):
    alphabet_topics = [item.lower() for item in make_url_by_alphabet()]
    res = requests.get(url)
    soup = BeautifulSoup(res.content, "lxml")
    topics = [(item.text, URL_BASE + item['href']) for item in soup.find_all('a', href=True)]
    result = []
    for text, url in topics:
        if (url in alphabet_topics or text == 'Topical\xa0Bible'):
            continue
        if (str(URL % '').lower() not in url.lower()):
            continue
        result.append((text, url))
    return result

if __name__ == "__main__":
    save_all_verses()