from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, String, Integer, Boolean
from sqlalchemy.orm import sessionmaker, Session
from sqlalchemy.sql.expression import func
from typing import List
from hashlib import md5

base = declarative_base()

def calculate_md5(params:List):
    str_params = [str(item) for item in params]
    value_to_md5 = '|'.join(str_params)
    return md5(value_to_md5.encode('utf-8')).hexdigest() 

class Verse(base):
    __tablename__ = 'Verse'

    verse_id = Column('verse_id', Integer, primary_key=True, autoincrement=True)
    verse = Column('verse', String, nullable=False, index=True, unique=True)
    hash_verse = Column('hash_verse', String, nullable=False, unique=True)
    text = Column('text', String, nullable=False, index=True, unique=True)
    hash_text = Column('hash_text', String, nullable=False, unique=True)
    status = Column('status', Boolean, default=True)
    all_len = Column('all_len', Integer, nullable=False)
    all_word_count = Column('all_word_count', Integer, nullable=False)

    def __init__(self, verse:str, text:str) -> None:
        self.verse = verse.lstrip().rstrip()
        self.hash_verse = calculate_md5(self.verse)
        self.text = text.lstrip().rstrip()
        self.hash_text = calculate_md5(self.text)
        self.all_len = len(self.text)
        self.all_word_count = len(self.text.split(' '))


class Topic(base):
    __tablename__ = 'Topic'

    topic_id = Column('topic_id', Integer, primary_key=True, autoincrement=True)
    topic = Column('topic', String, nullable=False, index=True)
    hash_topic = Column('hash_topic', String, nullable=False)
    verse_id = Column('verse_id', Integer, index=True, nullable=False)

    def __init__(self, topic:str, verse_id:int) -> None:
        self.topic = topic
        self.verse_id = verse_id
        self.hash_topic = calculate_md5(self.topic)


class Setting(base):
    __tablename__ = 'Setting'

    setting_id = Column('setting_id', Integer, primary_key=True, autoincrement=True)
    key = Column('key', String, nullable=False)
    value = Column('value', String, nullable=False)

    def __init__(self, key:str, value:str) -> None:
        self.key = key
        self.value = value

class NBibleDB():
    def __init__(self, path:str) -> None:
        self.path = path
        self.engine = create_engine('sqlite:///%s' % path, echo=True)
        try:
            base.metadata.create_all(self.engine)
        except Exception:
            pass
        self.sessionmaker = sessionmaker(bind=self.engine,autocommit=True)

    def insert_url_setting(self, url:str):
        session:Session = self.sessionmaker()
        element = session.query(Setting).filter_by(key=url).one_or_none()
        if element is not None:
            return
        session.add(Setting(url, 'TRUE'))

    def find_all_setting(self):
        session:Session = self.sessionmaker()
        elements = session.query(Setting).all()
        if elements is None or len(elements)<=0:
            return []
        return elements

    def contains_url_setting(self, url:str):
        session:Session = self.sessionmaker()
        element = session.query(Setting).filter_by(key=url).one_or_none()
        return element is not None

    def contains_setting(self, key:str, value:str):
        session:Session = self.sessionmaker()
        element = session.query(Setting).filter_by(key=key).filter_by(value=value).one_or_none()
        return element is not None

    def update_setting(self, key:str, value:str):
        session:Session = self.sessionmaker()
        session.query(Setting).filter_by(key=key).update({"value":value})

    def insert_collection(self, verses:List[Verse]):
        session:Session = self.sessionmaker()
        for verse in verses:
            element = session.query(Verse).filter_by(hash_text=verse.hash_text).one_or_none()    
            if element is not None:
                return
            session.add(verse)

    def find_all(self, limit:int=1000, random:bool=True):
        session:Session = self.sessionmaker()
        if random:
            elements = session.query(Verse).filter_by(status=True).order_by(func.random()).limit(limit).all()
        else:
            elements = session.query(Verse).filter_by(status=True).limit(limit).all()
        if elements is not None and len(elements) > 0:
            return elements

    def find_by_hash_text(self, hash:str):
        session:Session = self.sessionmaker()
        return session.query(Verse).filter_by(hash_text=hash).one_or_none()

    def find_topic_by_topic(self, topic:str, verse_id:int):
        session:Session = self.sessionmaker()
        element = session.query(Topic).filter_by(topic=topic).filter_by(verse_id=verse_id).one_or_none()
        return element

    def contains_topic(self, topic:str, verse_id:int):
        return self.find_topic_by_topic(topic, verse_id) is not None

    def insert_topic(self, topic:str, verse_id:int):
        session:Session = self.sessionmaker()
        element = self.find_topic_by_topic(topic, verse_id)
        if element is not None:
            return
        session.add(Topic(topic, verse_id))

    def clear_locket(self):
        session:Session = self.sessionmaker()
        # session.commit()
        session.expunge_all()
        session.close_all()