import requests
from bs4 import BeautifulSoup
from typing import List
from NSolganDB import NSloganDB

URL = "https://oberlo-growth-tools.shopifycloud.com/slogan/search?page={page_name}&term={search_text}"
DATABASE_PATH = "/home/rydzyk/Dokumenty/Projects/vsRepo/xnati/database/Slogan.sqllite"

db = NSloganDB(DATABASE_PATH)

siteid = db.get_or_insert_and_get_siteid(URL, "oberlo")

def get_slogans(texts:List[str]):
    for text in texts:
        print("Start scrappy: {text}".format(text=text))
        lower_text = text.lower()
        sloganwordid = db.get_or_insert_and_get_slangwordid(lower_text)
        for number in range(1, 200):
            url = make_url(number, text)
            slogans_from_one_page = get_slogans_from_page(url)
            if len(slogans_from_one_page) == 0:
                break
            for slogan in slogans_from_one_page:
                db.insert_slogan(slogan, siteid, sloganwordid)
            print("Inserted {number} slogans for text: {text}".format(number=len(slogans_from_one_page), text=text))

def make_url(number:int, text:str):
    search_value = text.replace(" ", "+")
    return URL.format(page_name=number, search_text=search_value)

def get_slogans_from_page(url) -> List[str]:
    result = []
    res = requests.get(url)
    soup = BeautifulSoup(res.content, "lxml")
    items = soup.find_all("span", class_="business-name")
    for item in items:
        if len(item.contents) > 0:
            result.append(item.contents[0])
    return result

def get_slogans_by_search_text(text:str):
    result = []
    for number in range(1, 200):
        url = make_url(number, text)
        slogans_from_one_page = get_slogans_from_page(url)
        if len(slogans_from_one_page) == 0:
            break
        result.append(slogans_from_one_page)
    return result

if __name__ == "__main__":
    input_texts = ["valentines day", "anime", "porno", "sex", "music", "cool", "movie", 
        "sport", "sports", "movies", "funny", "meme", "demon", "animal", "animals", "cat", 
        "cats", "funny accessories", "animals clothing", "funny clothing", "funny home-decor", 
        "laptop", "hoodies sweatshirts", "car", "computer", "water bottle", "coffee", "travel", 
        "phone", "journal", "erotic", "mask", "man", "woman", "win", "developer", 
        "software developer", "programming", "programmer", "alcohol", "beer", "all", "people", 
        "work", "job", "working", "love", "friendship", "hard work", "money", "make money", "earn", 
        "game", "life", "life game", "lifetime", "skill", "science", "learning", "languages", "language", 
        "freelance", "online", "offline", "work online", "apps", "app"]
    get_slogans(input_texts)