from sqlite3 import connect as open_sqlite
from typing import List, Dict, Tuple
from hashlib import md5

SQL_QUERY_CREATE_SITE_TABLE = \
    "CREATE TABLE IF NOT EXISTS SITE \
    ( \
        SITEID INTEGER PRIMARY KEY AUTOINCREMENT, \
        URL TEXT NOT NULL, \
        SITENAME TEXT NOT NULL, \
        UNIQUE(URL) \
    )"

SQL_QUERY_CREATE_SLOGAN_WORD_TABLE = \
    "CREATE TABLE IF NOT EXISTS SLOGANWORD \
    ( \
        SLOGANWORDID INTEGER PRIMARY KEY AUTOINCREMENT, \
        SLOGANWORD TEXT NOT NULL, \
        UNIQUE(SLOGANWORD) \
    )"

SQL_QUERY_CREATE_SLOGAN_TABLE = \
    "CREATE TABLE IF NOT EXISTS SLOGAN \
    ( \
        SLOGANID INTEGER PRIMARY KEY AUTOINCREMENT, \
        HASH TEXT NOT NULL, \
        SLOGAN TEXT NOT NULL, \
        SITEID INTEGER NOT NULL, \
        SLOGANWORDID INTEGRE NOT NULL, \
        STATUS INTEGER DEFAULT 0, \
        UNIQUE(HASH), \
        FOREIGN KEY(SITEID) REFERENCES SITE(SITEID), \
        FOREIGN KEY(SLOGANWORDID) REFERENCES SLOGANWORD(SLOGANWORDID) \
    )"

SQL_QUERY_SELECT_SLOGAN = \
    'SELECT SLOGAN FROM SLOGAN '

SQL_QUERY_SELECT_SITEID_BY_URL_AND_SITENAME = \
    'SELECT SITEID FROM SITE WHERE URL = ? AND SITENAME = ?'

SQL_QUERY_INSERT_SITE = \
    'INSERT OR REPLACE INTO SITE(URL, SITENAME) VALUES(?, ?)'

SQL_QUERY_INSERT_SLOGAN = \
    'INSERT OR REPLACE INTO SLOGAN(HASH, SLOGAN, SITEID, SLOGANWORDID) VALUES(?, ?, ?, ?)'

SQL_QUERY_INSERT_SLOGANTEXT = \
    'INSERT OR REPLACE INTO SLOGANWORD(SLOGANWORD) VALUES(?)'

SQL_QUERY_SELECT_SLOGANWORDID_BY_SLOGANWORD = \
    'SELECT SLOGANWORDID FROM SLOGANWORD WHERE SLOGANWORD = ?'

def calculate_md5(params:List):
    str_params = [str(item) for item in params]
    value_to_md5 = '|'.join(str_params)
    return md5(value_to_md5.encode('utf-8')).hexdigest() 

class NSloganDB():
    def __init__(self, path:str):
        self.path = path
        self.sql_driver = open_sqlite(path)
        self._create_structure_if_not_exists()

    def insert_slogan(self, slogan:str, siteid:int, sloganwordid:int):
        hash = calculate_md5([slogan])
        self._execute_sql_query(SQL_QUERY_INSERT_SLOGAN, [hash, slogan, siteid, sloganwordid])

    def get_or_insert_and_get_siteid(self, url:str, site_name:str):
        site_id = self.get_site_id(url, site_name)
        if (site_id is not None and site_id > 0):
            return site_id
        self.insert_site(url, site_name)
        return self.get_site_id(url, site_name)

    def insert_site(self, url:str, site_name:str):
        self._execute_sql_query(SQL_QUERY_INSERT_SITE, [url, site_name])

    def get_or_insert_and_get_slangwordid(self, slangword:str):
        slang_word_id = self.get_slang_word_id(slangword)
        if (slang_word_id is not None and slang_word_id > 0):
            return slang_word_id
        self.insert_slangword(slangword)
        return self.get_slang_word_id(slangword)

    def insert_slangword(self, slangword:str):
        self._execute_sql_query(SQL_QUERY_INSERT_SLOGANTEXT, [slangword])

    def get_slang_word_id(self, slangword:str):
        data = self._select_sql_query(SQL_QUERY_SELECT_SLOGANWORDID_BY_SLOGANWORD, [slangword])
        if len(data) > 0 and len(data[0]) > 0:
            siteid = data[0][0]
            return siteid
        return 0

    def get_site_id(self, url:str, site_name:str):
        data = self._select_sql_query(SQL_QUERY_SELECT_SITEID_BY_URL_AND_SITENAME, [url, site_name])
        if len(data) > 0 and len(data[0]) > 0:
            siteid = data[0][0]
            return siteid
        return 0

    def _create_structure_if_not_exists(self):
        cursor = self.sql_driver.cursor()
        cursor.executescript(SQL_QUERY_CREATE_SITE_TABLE)
        cursor.executescript(SQL_QUERY_CREATE_SLOGAN_WORD_TABLE)
        cursor.executescript(SQL_QUERY_CREATE_SLOGAN_TABLE)
        self.sql_driver.commit()
    
    def _execute_sql_query(self, sql_query:str, params:List):
        cursor = self.sql_driver.cursor()
        cursor.execute(sql_query, params)
        self.sql_driver.commit()

    def _select_sql_query(self, sql_query:str, params:List):
        cursor = self.sql_driver.cursor()
        cursor.execute(sql_query, params)
        return cursor.fetchall()