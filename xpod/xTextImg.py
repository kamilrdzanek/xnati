from tkinter import W
from typing import Tuple, List
from PIL import Image, ImageDraw, ImageFont
from PIL.ImageFont import FreeTypeFont
from os.path import join
from hashlib import md5
from xFonts import FONT_MAP
import os
import random


SIZE_BASE = (4500, 5400)
COLOR_BLACK = (0, 0, 0)
COLOR_TRANSPARENT = (0, 0, 0, 0)

OFFSET_VALUE = 0.1

FONTS_POD_PATH = '/home/rydzyk/Dokumenty/Projects/vsRepo/xnati/res/fonts_pod'
RESULT_PATH = "/home/rydzyk/Dokumenty/Projects/vsRepo/xnati/result/pod_img"

def get_fonts_pod():
    result_data = []
    for r, d, f in os.walk(FONTS_POD_PATH):
        for file in f:
            if file.endswith(".ttf") or file.endswith(".otf"):
                item = os.path.join(r, file)
                result_data.append(item)
    return result_data

FONTS_POD = get_fonts_pod()

def calculate_md5(params:List):
    str_params = [str(item) for item in params]
    value_to_md5 = '|'.join(str_params)
    return md5(value_to_md5.encode('utf-8')).hexdigest() 

def find_font_size(width:int, text:str, font_path:str, size:int=3000):
    def fit(width, font_size, text):
        font = ImageFont.truetype(font_path, font_size)
        text_w, text_h = font.getsize(text)
        if width>text_w:
            return (font_size, text_w, text_h, True)
        else:
            scale = width/text_w
            font_size = int(round(font_size*scale))
            return (font_size, text_w, text_h, False)
    font_size = size
    while True:
        size_value, w, h, status = fit(width, font_size, text)
        if status == True: 
            break
        else:
            font_size = size_value - 1
    return (font_size, w, h)

def make_image_text_with_wrap(text:str, 
                              font:FreeTypeFont, 
                              fill:Tuple[int,int,int], 
                              img_size:Tuple[int,int]=SIZE_BASE,
                              align_text:str='center',
                              offset:float=0.1):
    x1 = img_size[0] - int(img_size[0] * offset)
    y1 = img_size[1] - int(img_size[1] * offset)

    img = Image.new('RGBA', (x1, y1), COLOR_TRANSPARENT)
    d = ImageDraw.Draw(img)
    quote = text

    sum_value = 0
    for letter in quote:
        sum_value += d.textsize(letter, font=font)[0]
    average_length_of_letter = sum_value / len(quote)
   
    number_of_letters_for_each_line = (x1 / 1.618) / average_length_of_letter
    incrementer = 0
    fresh_sentence = ''

    for letter in quote:
        if letter == '-':
            fresh_sentence += '\n\n' + letter
        elif incrementer < number_of_letters_for_each_line:
            fresh_sentence += letter
        else:
            if letter == ' ':
                fresh_sentence += '\n'
                incrementer = 0
            else:
                fresh_sentence += letter
        incrementer += 1

    dim = d.textsize(fresh_sentence, font=font)
    x2 = dim[0]
    y2 = dim[1]
    qx = (x1 / 2 - x2 / 2)
    qy = (y1 / 2 - y2 / 2)
    d.text((qx, qy), fresh_sentence, align=align_text, font=font, fill=fill)

    return img

def wrapp_text_by_words(test:str):
    words = [w for w in test.split(' ') if w is not None and len(w)>0]
    words_result = []
    tmp_value = ''
    for w in words:
        if len(w) < 3 or w.lower() == 'the':
            tmp_value += '%s ' % w
        else:
            tmp_value += w
            words_result.append(tmp_value)
            tmp_value = ''
    return words_result

def make_text_image(text:str, img_size=SIZE_BASE):
        make_text_image_style_1(text, img_size, upper_text=False)
        make_text_image_style_1(text, img_size, upper_text=True)
        make_text_image_style_2(text, img_size, upper_text=False)
        make_text_image_style_2(text, img_size, upper_text=True)
        make_text_image_style_3(text, img_size, upper_text=False)
        make_text_image_style_3(text, img_size, upper_text=True)
        make_text_image_style_4(text, img_size, upper_text=False)

def make_text_image_style_1(text:str, img_size=SIZE_BASE, font_paths=FONTS_POD, upper_text:bool=True):
    width_img:int = img_size[0] - int(img_size[0] * OFFSET_VALUE)
    for font_path in font_paths:
        text_value = text.upper() if upper_text else text
        font_size, w, h = find_font_size(width_img, text_value, font_path)
        fnt = ImageFont.truetype(font_path, font_size)
        img = Image.new('RGBA', img_size, COLOR_TRANSPARENT)
        d = ImageDraw.Draw(img)
        width_text, height_text = d.textsize(text_value, fnt)
        qx = int(round((img_size[0] - width_text) / 2))
        qy = int(round((img_size[1] - height_text) / 2))
        d.text((qx, qy), text_value, COLOR_BLACK, fnt)
        font_name = "%s_%s" % fnt.getname()
        hash_value = calculate_md5([font_name, text_value])
        location = join(RESULT_PATH, "%s.png" % hash_value)
        img.save(location)

def make_text_image_style_2(text:str, img_size=SIZE_BASE, font_paths=FONTS_POD, upper_text:bool=True, font_size:int=550):
    # width_img:int = img_size[0] - int(img_size[0] * OFFSET_VALUE)
    for font_path in font_paths:
        text_value = text.upper() if upper_text else text
        fnt = ImageFont.truetype(font_path, font_size)
        img = make_image_text_with_wrap(text, fnt, COLOR_BLACK, img_size=img_size, offset=OFFSET_VALUE)
        font_name = "%s_%s" % fnt.getname()
        hash_value = calculate_md5([font_name, text_value])
        location = join(RESULT_PATH, "%s.png" % hash_value)
        img.save(location)

def make_text_image_style_3(text:str, 
                            img_size=SIZE_BASE, 
                            font_paths=FONTS_POD, 
                            upper_text:bool=True, 
                            text_number:int=6, 
                            nh_value:float=None):
    width_img:int = img_size[0] - int(img_size[0] * OFFSET_VALUE)
    font_paths.sort()
    for font_path in font_paths:
        newh_value = 0.3
        font_file_name = os.path.basename(font_path)
        f_setting = FONT_MAP.get(font_file_name)
        if (f_setting is not None and f_setting.style_3 == False):
            continue
        if (f_setting is not None):
            newh_value = f_setting.s3_newh_value
        if (nh_value is not None):
            newh_value = nh_value
        text_value = text.upper() if upper_text else text
        font_size, w, h = find_font_size(width_img, text_value, font_path)
        fnt = ImageFont.truetype(font_path, font_size)
        images = []
        normal_text = 2 if random.uniform(0,1) < 0.5 else 3
        for img_index in range(text_number):
            start_w = int(round(w * 0.1 / 2))
            start_h = int(round(h * newh_value / 2))
            new_w = w + start_w * 2
            new_h = h + start_h * 2
            img_text = Image.new('RGBA', (new_w, new_h), COLOR_TRANSPARENT)
            d = ImageDraw.Draw(img_text)
            if (img_index == text_number - normal_text):
                d.text((start_w, start_h), text, fill=COLOR_BLACK, font=fnt, align = 'center',
                stroke_width=2, stroke_fill=COLOR_BLACK)
            else:
                d.text((start_w, start_h), text, fill=COLOR_TRANSPARENT, font=fnt, align = 'center',
                stroke_width=20, stroke_fill=COLOR_BLACK)
            images.append(img_text)
        img = Image.new('RGBA', img_size, COLOR_TRANSPARENT)
        final_start_h = int(round((img_size[1] - new_h * text_number) / 2))
        for image in images:
            img.paste(image, (0, final_start_h))
            final_start_h += new_h
        hash_value = calculate_md5([text_value])
        location = join(RESULT_PATH, "%s %s.png" % (font_file_name, hash_value))
        img.save(location)

def make_text_image_style_4(text:str, 
                            img_size=SIZE_BASE, 
                            font_paths=FONTS_POD, 
                            upper_text:bool=True):
    newh_value = 0.3
    text_value = text.upper() if upper_text else text
    parts = wrapp_text_by_words(text_value) 
    width_img:int = img_size[0] - int(img_size[0] * OFFSET_VALUE)
    font_paths.sort()
    for font_path in font_paths:
        font_file_name = os.path.basename(font_path)
        images = []
        tmp_h = 0
        for w_part in parts:
            font_size, w, h = find_font_size(width_img, w_part, font_path)
            fnt = ImageFont.truetype(font_path, font_size)
            start_w = int(round(w * 0.1 / 2))
            start_h = int(round(h * newh_value / 2))
            new_w = w + start_w * 2
            new_h = h + start_h * 2
            tmp_h += new_h
            img_text = Image.new('RGBA', (new_w, new_h), COLOR_TRANSPARENT)
            d = ImageDraw.Draw(img_text)
            d.text((start_w, start_h), w_part, fill=COLOR_BLACK, font=fnt, align = 'center',
            stroke_width=2, stroke_fill=COLOR_BLACK)
            images.append((img_text, new_h))
        img = Image.new('RGBA', img_size, COLOR_TRANSPARENT)
        final_start_h = int(round((img_size[1] - tmp_h) / 2))
        for image, h in images:
            img.paste(image, (0, final_start_h))
            final_start_h += h
        hash_value = calculate_md5([text_value])
        location = join(RESULT_PATH, "%s %s.png" % (font_file_name, hash_value))
        img.save(location)

if __name__ == "__main__":
    make_text_image("Coffee Lover Real Name")
