from sqlite3 import connect as open_sqlite
from typing import List, Dict, Tuple
from hashlib import md5

from sqlalchemy import true

SQL_QUERY_CREATE_SITE_TABLE = \
    "CREATE TABLE IF NOT EXISTS SITE \
    ( \
        SITEID INTEGER PRIMARY KEY AUTOINCREMENT, \
        URL TEXT NOT NULL, \
        SITENAME TEXT NOT NULL, \
        UNIQUE(URL) \
    )"

SQL_QUERY_CREATE_SLOGAN_WORD_TABLE = \
    "CREATE TABLE IF NOT EXISTS SLOGANWORD \
    ( \
        SLOGANWORDID INTEGER PRIMARY KEY AUTOINCREMENT, \
        SLOGANWORD TEXT NOT NULL, \
        UNIQUE(SLOGANWORD) \
    )"

SQL_QUERY_CREATE_SLOGAN_TABLE = \
    "CREATE TABLE IF NOT EXISTS SLOGAN \
    ( \
        SLOGANID INTEGER PRIMARY KEY AUTOINCREMENT, \
        HASH TEXT NOT NULL, \
        SLOGAN TEXT NOT NULL, \
        SITEID INTEGER NOT NULL, \
        SLOGANWORDID INTEGRE NOT NULL, \
        STATUS INTEGER DEFAULT 0, \
        UNIQUE(HASH), \
        FOREIGN KEY(SITEID) REFERENCES SITE(SITEID), \
        FOREIGN KEY(SLOGANWORDID) REFERENCES SLOGANWORD(SLOGANWORDID) \
    )"

SQL_QUERY_SELECT_SLOGAN = \
    'SELECT S.SLOGANID, S.HASH, S.SLOGAN, SW.SLOGANWORD \
    FROM SLOGAN S \
    JOIN SLOGANWORD SW ON S.SLOGANWORDID = SW.SLOGANWORDID \
    #rand# #limit#'

class SloganItem():
    def __init__(self, slogan_id:int, hash:str, slogan:str, word:str) -> None:
        self.slogan_id = slogan_id
        self.hash = hash
        self.slogan = slogan
        self.word = word

class XSloganDB():
    def __init__(self, path:str):
        self.path = path
        self.sql_driver = open_sqlite(path)

    def find_random_slogan(self, limit:int=0, random:bool=true):
        result = []
        query = SQL_QUERY_SELECT_SLOGAN \
            .replace("#rand#", "ORDER BY RANDOM()" if random else "") \
            .replace("#limit#", ("LIMIT %s" % limit) if limit>0 else "")
        data = self._select_sql_query(query, [])
        if len(data) > 0:
            for index, row in enumerate(data):
                slogan_id:int = row[0]
                hash_value:str = row[1]
                slogan:str = row[2]
                word:str = row[3]
                if (slogan_id > 0):
                    item = SloganItem(slogan_id, hash_value, slogan, word)
                    result.append(item)
        return result

    
    def _execute_sql_query(self, sql_query:str, params:List):
        cursor = self.sql_driver.cursor()
        cursor.execute(sql_query, params)
        self.sql_driver.commit()

    def _select_sql_query(self, sql_query:str, params:List):
        cursor = self.sql_driver.cursor()
        cursor.execute(sql_query, params)
        return cursor.fetchall()