from typing import Tuple
from xSolganDB import XSloganDB, SloganItem
from PIL import Image, ImageDraw, ImageFont
from PIL.ImageFont import FreeTypeFont
from uuid import uuid4
from os.path import join
import os
import random
import textwrap
import shutil

DATABASE_PATH = "/home/rydzyk/Dokumenty/Projects/vsRepo/xnati/database/Slogan.sqllite"
FONT_PATH = "/home/rydzyk/Dokumenty/Projects/vsRepo/xnati/res/fonts"
RESULT_PATH = "/home/rydzyk/Dokumenty/Projects/vsRepo/xnati/result/pod_img"

db = XSloganDB(DATABASE_PATH)

FONTS = [join(FONT_PATH, 'tillana-extrabold.ttf')]
FONTS_POD_PATH = '/home/rydzyk/Dokumenty/Projects/vsRepo/xnati/res/fonts_pod'

def get_fonts_pod():
    result_data = []
    for r, d, f in os.walk(FONTS_POD_PATH):
        for file in f:
            if file.endswith(".ttf") or file.endswith(".otf"):
                item = os.path.join(r, file)
                result_data.append(item)
    return result_data

FONTS_POD = get_fonts_pod()

def aling_text(font_size, text):
    wrapper = textwrap.TextWrapper(width = font_size)
    text_element = wrapper.wrap(text = text)
    return text_element

def make_images(number_of_images:int = 1):
    items = db.find_random_slogan(number_of_images)
    for item in items:
        element:SloganItem = item
        if (element.slogan.endswith('.')):
            element.slogan = element.slogan[:len(element.slogan)-1]
        # make_image_algorythm_1(item)
        # make_image_algorythm_2(item)
        # make_image_algorythm_3(item)

def make_image_algorythm_1(item:SloganItem):
    for f_path in FONTS_POD:
        fnt = ImageFont.truetype(f_path, 150)
        font_name = "%s_%s" % fnt.getname()
        out_file_name = "%s_%s" % (font_name,str(uuid4()).replace('-','')[:5])
        dir_fname = font_name.replace(' ', '_')
        result_fpath = os.path.join(RESULT_PATH, dir_fname)
        if (os.path.isdir(result_fpath) == False):
            os.mkdir(result_fpath)
        new_out_file_name = os.path.join(dir_fname, out_file_name)
        try:
            make_image_text_with_wrap(item, fnt, (0, 0, 0), new_out_file_name)
        except Exception:
            print("Font: %s | Slogan: %s" % (font_name, item.slogan))
        f_name = os.path.basename(f_path)
        new_f_path = os.path.join(result_fpath, f_name)
        if (os.path.isfile(new_f_path) == False):
            shutil.copy2(f_path, new_f_path)

def make_image_algorythm_2(item:SloganItem):
    font_path = random.choice(FONTS_POD)
    fnt = ImageFont.truetype(font_path, 150)
    font_name = "%s_%s" % fnt.getname()
    out_file_name = "%s_%s" % (font_name,str(uuid4()).replace('-','')[:8])
    make_image_text_with_wrap(item, fnt, (0, 0, 0), out_file_name) 

def make_image_text_with_wrap(item:SloganItem, 
                              font:FreeTypeFont, 
                              fill:Tuple[int,int,int], 
                              out_file_name:str,
                              img_size:Tuple[int,int]=(1500,1500),
                              align_text:str='center'):
    x1, y1 = img_size

    img = Image.new('RGBA', (x1, y1), (0, 0, 0, 0))
    d = ImageDraw.Draw(img)
    quote = item.slogan

    sum_value = 0
    for letter in quote:
        sum_value += d.textsize(letter, font=font)[0]
    average_length_of_letter = sum_value / len(quote)
   
    number_of_letters_for_each_line = (x1 / 1.618) / average_length_of_letter
    incrementer = 0
    fresh_sentence = ''

    for letter in quote:
        if letter == '-':
            fresh_sentence += '\n\n' + letter
        elif incrementer < number_of_letters_for_each_line:
            fresh_sentence += letter
        else:
            if letter == ' ':
                fresh_sentence += '\n'
                incrementer = 0
            else:
                fresh_sentence += letter
        incrementer += 1

    dim = d.textsize(fresh_sentence, font=font)
    x2 = dim[0]
    y2 = dim[1]
    qx = (x1 / 2 - x2 / 2)
    qy = (y1 / 2 - y2 / 2)
    d.text((qx, qy), fresh_sentence, align=align_text, font=font, fill=fill)

    location = join(RESULT_PATH, "%s.png" % out_file_name)
    img.save(location)

def make_image_algorythm_3(item:SloganItem):
    width = 1500
    height = 1500
    fnt = ImageFont.truetype(FONTS[0], 150)
    img = Image.new('RGBA', (width, height), (0, 0, 0, 0))
    d = ImageDraw.Draw(img)
    words = item.slogan.split(' ')
    word_array = []
    tmp_word = ''
    for word in words:
        if (len(word) > 2 and tmp_word == ''):
            word_array.append(word)
            tmp_word = ''
        elif (tmp_word != ''):
            word_array.append("%s %s" % (tmp_word, word))
            tmp_word = ''
        else:
            tmp_word = word
    words.clear()
    font_size = find_font_size(width, height)

def find_font_size(width:int, text:str, font_path:str=FONTS[0], size:int=150):
    def fit(width, font_size, text):
        font = ImageFont.truetype(font_path, font_size)
        text_w, text_h = font.getsize(text)
        if width>text_w:
            return (font_size, text_w, text_h, True)
        else:
            scale = width/text_w
            font_size = int(round(font_size*scale))
            return (font_size, text_w, text_h, False)
    font_size = size
    while True:
        size_value, w, h, status = fit(width, font_size, text)
        if status == True: 
            break
        else:
            font_size = size_value - 1
    return (font_size, w, h)

if __name__ == "__main__":
    # items = db.find_random_slogan(2)
    # value = find_font_size(1500, 1500, 'Animals enjoy when no-ones around Animals enjoy when no-ones around')
    # print(value)
    # for item in items:
    #     make_image_algorythm_3(item)
    # make_images(10)

    tmp = []
    tmp1 = FONTS_POD
    tmp1.sort()
    for f_path in tmp1:
        fnt = ImageFont.truetype(f_path, 150)
        font_name = "%s_%s" % fnt.getname()
        file_name = os.path.basename(f_path)
        tmp.append(font_name + '\n')
        tmp.append(file_name + '\n')
        if ('-' in file_name):
            tmp.append(file_name[:str(file_name).index('-')] + '\n')
        else:
            tmp.append(file_name[:str(file_name).index('.')] + '\n')
        tmp.append(' \n')

    with open('tmp.txt', 'w') as f:
        f.writelines(tmp)

    # width = 1500
    # width_text = 1000
    # height = 1500
    
    # caption = 'Ans: stroke_width & stroke_fill'

    # size, w, h = find_font_size(width_text, caption)
    # fnt = ImageFont.truetype(FONTS[0], size)

    # images = []
    # new_w = int(round(w + w * 10 / 100))
    # start_w = int(round((new_w - w) / 2))
    # for index in range(10):
    #     img = Image.new('RGBA', (new_w, h), (0, 0, 0, 0))
    #     d = ImageDraw.Draw(img)
    #     if (index == 8):
    #         d.text((start_w, 0), caption, fill='black', font=fnt, align = 'center',
    #             stroke_width=1, stroke_fill='black')
    #     else:    
    #         d.text((start_w, 0), caption, fill=(0, 0, 0, 0), font=fnt, spacing = 4, align = 'center',
    #             stroke_width=4, stroke_fill='black')
    #     images.append(img)
    
    # new_h = len(images) * h

    # new_img = Image.new('RGBA', (new_w, new_h), (0, 0, 0, 0))
    # tmp_h = 0
    # for image in images:
    #     new_img.paste(image, (0, tmp_h))
    #     tmp_h += h

    # final_img = Image.new('RGBA', (width, height), (0, 0, 0, 0))
    # start_w = int(round((width - new_img.width) / 2))
    # start_h = int(round((height - new_img.height) / 2))
    # final_img.paste(new_img, (start_w, start_h))

    # final_img.show()