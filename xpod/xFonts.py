from sqlalchemy import false


class XFontSetting():
    def __init__(self, 
                 enable:bool=True, 
                 style_3:bool=True,
                 style_4:bool=True,
                 only_upper:bool=True,
                 s3_stroke_width:int=2, 
                 s3_ol_stroke_width:int=20,
                 s3_newh_value:float=0.3
                 ) -> None:
        self.enable = enable
        self.style_3 = style_3
        self.style_4 = style_4
        self.only_upper = only_upper
        self.s3_stroke_width = s3_stroke_width
        self.s3_ol_stroke_width = s3_ol_stroke_width #outline
        self.s3_newh_value = s3_newh_value

FONT_MAP = {
    'AAbsoluteEmpire-axyaJ.otf':XFontSetting(s3_newh_value=1),
    'AAhaWow-2O1K8.ttf':XFontSetting(s3_newh_value=0.8),
    'AAntiCorona-L3Ax3.ttf':XFontSetting(only_upper=False),
    'AAsianHiroCyr-VGJVZ.otf':XFontSetting(style_4=False),
    'Ablasco-51apa.ttf':XFontSetting(s3_newh_value=0.4, style_4=False),
    'Abrasion-6YZd1.otf':XFontSetting(s3_newh_value=0.8),
    'Abrushow-ALwDD.ttf':XFontSetting(s3_newh_value=0.8, style_4=False),
    'AbstractGroovy-K7Zdy.ttf':XFontSetting(s3_newh_value=1),
    'AfricanCulture-J17a.ttf':XFontSetting(style_3=False, only_upper=False),
    'AgenGiveaway-EaGRW.otf':XFontSetting(style_3=False, style_4=False, only_upper=False),
    'AgustusMerdeka-4BgP4.otf':XFontSetting(s3_newh_value=0.8),
    'AjaMales-K7ZwZ.ttf':XFontSetting(s3_newh_value=0.8),
    'AkarRumput-JRZzx.ttf':XFontSetting(s3_newh_value=0.8),
    'AksiMosi-4BRYB.otf':XFontSetting(s3_newh_value=0.8),
    'Akuilah-lgBeZ.otf':XFontSetting(s3_newh_value=1),
    'Alphakind-gxyM5.ttf':XFontSetting(style_4=False),
    'AmneSans-RpXxA.otf':XFontSetting(style_4=False, style_3=False, only_upper=False),
    'AmpunBang-L3Z7G.ttf':XFontSetting(s3_newh_value=1),
    'AndromedaNormal-ja30.ttf':XFontSetting(style_3=False),
    'Anggota-d9v9E.otf':XFontSetting(only_upper=False, style_4=False),
    'AngkanyaSebelas-VGPDB.ttf':XFontSetting(style_3=False, style_4=False),
    'AngkatanBersenjata-2OD4o.ttf':XFontSetting(s3_newh_value=1),
    'AnomanObong-GOZJD.otf':XFontSetting(only_upper=False, style_4=False, style_3=False),
    'ApeMount-eZj0O.otf':XFontSetting(s3_newh_value=0.8),
    'AreaStencil-MVWmB.ttf':XFontSetting(s3_newh_value=1),
    'ArtPaper-AL0Jp.otf':XFontSetting(s3_newh_value=0.8),
    'AssassinNinja-w12j8.otf':XFontSetting(only_upper=False, style_4=False, style_3=False),
    'AstroSpace-eZ2Bg.ttf':XFontSetting(s3_newh_value=1),
    'AtariClassic-gry3.ttf':XFontSetting(s3_newh_value=0.8),
    'AtaxiaOutlineBrk-2dol.ttf':XFontSetting(only_upper=False, style_4=False, style_3=False),
    'Atos-RpXZE.otf':XFontSetting(),

    'BackoffShadow-59ex.ttf':XFontSetting(style_3=False, style_4=False, only_upper=False),
    'BadComic-VGD90.ttf':XFontSetting(style_4=False, only_upper=False),
    'BadMofo-xLa0.ttf':XFontSetting(style_3=False, style_4=False, only_upper=False),
    'Bangers-YPnj.ttf':XFontSetting(s3_newh_value=0.4, style_4=False),
    'Bestagon-Thick.otf':XFontSetting(s3_newh_value=0.5, style_4=False),
    'BigBlocko-aEOE.ttf':XFontSetting(style_3=False, style_4=False),
    'Bimasakti-wjoZ.otf':XFontSetting(s3_newh_value=0.5),
    'BlockCartoon-jEY0j.ttf':XFontSetting(s3_newh_value=1),
    'BlockHelvetica-gxpM6.ttf':XFontSetting(),
    'BlockwayRegular-0p5G.ttf':XFontSetting(),
    'BlueberryCupcake-OVy98.ttf':XFontSetting(style_4=False),
    'BpdotsunicaseminusBold-0ORr.otf':XFontSetting(s3_newh_value=0.5),
    'Bpdotsunicasesquare-XqK2.otf':XFontSetting(s3_newh_value=0.5),
    'BroshkLime-BWL5n.ttf':XFontSetting(),
    'Burukuduk-2O71o.otf':XFontSetting(s3_newh_value=0.8, only_upper=false),
    'BuzluBuzRegular-jEv9v.ttf':XFontSetting(style_4=False, only_upper=False),

    'CarnivalmfOpenshadow-ld5w.ttf':XFontSetting(s3_newh_value=0.4),
    'Cartoon1471Extended-x3oyq.ttf':XFontSetting(only_upper=False),
    'Catcafe-v5yM.ttf':XFontSetting(),
    'Cespleng-OVp6O.otf':XFontSetting(s3_newh_value=0.8),
    'CgfLocustResistance-1Goj4.ttf':XFontSetting(s3_newh_value=0.8),
    'ChaleBrush.otf':XFontSetting(only_upper=False),
    'CherryCocktail-DOpY9.ttf':XFontSetting(only_upper=False),
    'ChristopherDone-mAWm.ttf':XFontSetting(only_upper=False),
    'Chubb-jYLy.ttf':XFontSetting(style_3=False),
    'CoconutHouse-9YG0y.otf':XFontSetting(s3_newh_value=0.5),
    'CollegiateheavyoutlineMedium-B0yx.ttf':XFontSetting(s3_newh_value=0.6, only_upper=False),
    'ComicShark-lgVE5.otf':XFontSetting(s3_newh_value=0.4),
    'Comiczineot-XEgK.otf':XFontSetting(style_3=False),
    'CursedTimerUlil-Aznm.ttf':XFontSetting(s3_newh_value=1, style_4=False),
    'CgfOffRoad-Rp39l.ttf':XFontSetting(style_3=False),

    'Dagsen_Black.otf':XFontSetting(s3_newh_value=1.2),
    'Dagsen_Outline.otf':XFontSetting(style_3=False),
    'Disc-woO3.ttf':XFontSetting(only_upper=False, style_3=False),
    'DistroBev-LK4.ttf':XFontSetting(only_upper=False),
    'DistroBold-gR6.ttf':XFontSetting(only_upper=False),
    'DistroToast-Wmv.ttf':XFontSetting(only_upper=False, style_3=False),
    'DistroVinyl-aM9.ttf':XFontSetting(style_3=False, only_upper=False),
    'Ducktales-qnjd.ttf':XFontSetting(s3_newh_value=1),
    'DuoDunkel-ywee.ttf':XFontSetting(s3_newh_value=1),
    'DuoLicht-9Ygn.ttf':XFontSetting(s3_newh_value=1),

    'ErbosDraco1StNbpRegular-99V5.ttf':XFontSetting(style_3=False, only_upper=False),
    'Estrogen-YJL.ttf':XFontSetting(s3_newh_value=0.5, only_upper=False),
    'Euphoric3DBrk-MrKp.ttf':XFontSetting(only_upper=False),

    'FaintBookRegular-YzWEy.ttf':XFontSetting(s3_newh_value=0.5),
    'FaulmannFont-lg7Mw.ttf':XFontSetting(s3_newh_value=0.1, only_upper=False),
    'Fegan-GOaGm.ttf':XFontSetting(only_upper=False),
    'Fifaks10Dev1-K7avW.ttf':XFontSetting(),
    'Flicker15-mLqm.ttf':XFontSetting(s3_newh_value=1),
    'Fontania-MjXr.ttf':XFontSetting(only_upper=False),
    'Freestyle-vmA3Z.ttf':XFontSetting(only_upper=False),
    'Freshman-POdx.ttf':XFontSetting(s3_newh_value=0.7, only_upper=False),
    'FriendlyComics-RpX9A.otf':XFontSetting(),
    'FtBlockbusta-4y36.ttf':XFontSetting(style_3=False),

    'GallagherGallagher-l31d.ttf':XFontSetting(style_3=False, only_upper=False),
    'GhiyaStrokes-BEZl.ttf':XFontSetting(only_upper=False),
    'GomaBlock-V1Bx.ttf':XFontSetting(s3_newh_value=0.7),
    'Graffitipaintbrush-21Jv.ttf':XFontSetting(s3_newh_value=0.7),

    'Hacked-KerX.ttf':XFontSetting(s3_newh_value=0.5),
    'HussarMilosc-lO6e.otf':XFontSetting(only_upper=False),
    'HussarMiloscOblique-n3lJ.otf':XFontSetting(only_upper=False),

    'ImpactLabel-lVYZ.ttf':XFontSetting(style_3=False),
    'ImpactLabelReversed-nDMR.ttf':XFontSetting(style_3=False),

    'Jingleberry-4wVp.otf':XFontSetting(only_upper=False),

    'KappasPororoca-B83G.ttf':XFontSetting(s3_newh_value=0.7, only_upper=False),
    'KestoyRock-2wmo.ttf':XFontSetting(only_upper=False),
    'KeyVirtue-4B3W9.ttf':XFontSetting(s3_newh_value=0.1, only_upper=False),
    'KnobberRegular-ywnYe.ttf':XFontSetting(s3_newh_value=0.5),

    'LackawannaWeedNf-jWgy.ttf':XFontSetting(only_upper=False, style_3=False),
    'LadyIce3D-7KE.ttf':XFontSetting(s3_newh_value=0.2, only_upper=False, style_3=False),
    'LadyIce3DItalic-myV.ttf':XFontSetting(s3_newh_value=0.2, only_upper=False, style_3=False),
    'Laffriotnf-z0DL.ttf':XFontSetting(),
    'Library3am-5V3Z.otf':XFontSetting(s3_newh_value=0.8),
    'Lucy-MAYP.ttf':XFontSetting(only_upper=False),
    'Lunch-Yq4y.ttf':XFontSetting(s3_newh_value=0.8),

    'Manils-MVr8v.otf':XFontSetting(s3_newh_value=1),
    'Messages-gnn5.ttf':XFontSetting(style_3=False),
    'MessagesItalic-oKKA.ttf':XFontSetting(s3_newh_value=0.5),
    'MessyLetters-eZjYg.otf':XFontSetting(s3_newh_value=0.1, only_upper=False),
    'Midnight-MVYWp.ttf':XFontSetting(),
    'MyUnderwood-KOVy.ttf':XFontSetting(s3_newh_value=0.7, only_upper=False),

    'NeonSans-L3GXE.ttf':XFontSetting(style_3=False),
    'NorseBold-2Kge.otf':XFontSetting(s3_newh_value=0.4, style_3=False),
    'NothingForYou-GOO1y.otf':XFontSetting(s3_newh_value=0.4),
    'NoVirus-RpyYo.ttf':XFontSetting(s3_newh_value=0.4, only_upper=False),

    'OldPrintingPressFreeVersion-8jd0.ttf':XFontSetting(s3_newh_value=0.5, only_upper=False, style_3=False),
    'Oregano-l5Ky.ttf':XFontSetting(s3_newh_value=0.1, only_upper=False),

    'Paintingwithchocolate-K5mo.ttf':XFontSetting(style_3=False, only_upper=False),
    'Platsch2-6R5Y.ttf':XFontSetting(style_3=False),
    'Platsch-5y58.ttf':XFontSetting(style_3=False, only_upper=False),
    'Plok-vnWy.ttf':XFontSetting(s3_newh_value=1),

    'QuickPencilRegular-0R59.ttf':XFontSetting(only_upper=False),
    
    'RedOut-PL5r.ttf':XFontSetting(s3_newh_value=0.5),
    'RichieBrusher-l2yy.ttf':XFontSetting(only_upper=False),
    'RocketMission-Rpnoo.otf':XFontSetting(only_upper=False),

    'Seasideresortnf-popd.ttf':XFontSetting(style_3=False),
    'Semongko-L3ZO3.ttf':XFontSetting(only_upper=False),
    'ShibuyaZero-R2ee.ttf':XFontSetting(s3_newh_value=1.2),
    'Shojumaru-6ZlM.ttf':XFontSetting(s3_newh_value=0.5, only_upper=False),
    'SlimeBox-GWMD.ttf':XFontSetting(only_upper=False),

    'ToujoursRegular-BWezl.ttf':XFontSetting(s3_newh_value=1.2),
    'ToylandNf-nDoR.ttf':XFontSetting(s3_newh_value=0.7),
    'Troubleside-lgjxX.ttf':XFontSetting(s3_newh_value=0.5),
    'TrueCrimes-XGwj.ttf':XFontSetting(s3_newh_value=0.5, only_upper=False),

    'Waliny-K7d37.ttf':XFontSetting(s3_newh_value=0.2, only_upper=False),
    'WashYourHand-d9xaV.otf':XFontSetting(only_upper=False),
    'Whypo-rrey.ttf':XFontSetting(s3_newh_value=1),
    'WildBlackItalic-7dLl.ttf':XFontSetting(only_upper=False),
    'WildBlack-vMoD.ttf':XFontSetting(only_upper=False),
}

